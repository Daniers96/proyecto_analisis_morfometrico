#Importar las librerias

import numpy as np
import os
import shutil
import matplotlib.pyplot as plt 
import csv
import statistics as statis
import math

from matplotlib.pyplot import hist, show
from matplotlib.colors import LinearSegmentedColormap
from scipy import ndimage, stats
from skimage import morphology, data, exposure, img_as_float
from skimage import io, color, filters, util, measure
from skimage.measure import regionprops, label
from skimage.feature import peak_local_max
from skimage.filters import threshold_otsu, threshold_yen, threshold_sauvola, threshold_niblack
from skimage.color import rgb2hed, rgb2gray, label2rgb
from skimage.morphology import binary_erosion, binary_dilation, binary_opening, binary_closing
from skimage.morphology import erosion, dilation, opening, closing, white_tophat, watershed, black_tophat
from skimage.morphology import disk, rectangle, diamond, square

raiz='hem/40X' #directorio raiz

#listar imagenes

def listar(de):
    #Variable para la ruta al directorio
    path = de
 
    #Lista vacia para incluir los ficheros
    name = []
 
    #Lista con todos los ficheros del directorio:
    lstDir = os.walk(path)   #os.walk() Lista directorios y ficheros
    #Crea una lista de los ficheros png que existen en el directorio y los incluye a la lista.
    i=0
    for root, dirs, files in lstDir: # recorre el direcctorio sacando los datos de las carpetas
        for fichero in files:
            (nombreFichero, extension) = os.path.splitext(fichero)
    
            if(extension == ".png"):
                name.append(nombreFichero)
                
        i += 1
        if i >= 1:
            break # rompe el ciclo para poder sacar solamente lo de la carpeta principal
    return name

def conver1(val, raiz):
    sol=0
    if(raiz=='mac/20X'):
        sol=(2720.6656*val)/303623.0404
    if(raiz=='mac/40X'):
        sol=(708.0921*val)/303623.0404
    return sol

def conver2(val, raiz):
    sol=0
    if(raiz=='hem/20X'):
        sol=(52.16*val)/551.02
    if(raiz=='hem/40X'):
        sol=(26.61*val)/551.02
    return sol

#Funcion para Imprimir
def plot_comparison(original, filtered):

    io.imshow(original)
    io.show()
    io.imshow(filtered)
    io.show()
    
#linealizacion
def linealizar():
    path=raiz#directorio de trabajo

    name = listar(raiz) # entra a la funcion para sacar los archivos .png

    nuevaruta = r'hem/40x/linealizacion' #Ruta directorio
    if not os.path.exists(nuevaruta): os.makedirs(nuevaruta) #Validar y crear directorio

    for i in range(len(name)):
        img = io.imread(raiz+"/"+name[i]+'.png')  #leer Imagen 

        histo=exposure.histogram(img[:, :, 0]) #histograma de R

        a=histo[1].min() #valor minimo del histograma
        a1=histo[1].max() #valo maximo del histograma

        histo=exposure.histogram(img[:, :, 1]) #histograma de G

        b=histo[1].min() #valor minimo del histograma
        b1=histo[1].max() #valor maximo del histograma

        histo=exposure.histogram(img[:, :, 2]) #histograma de B

        c=histo[1].min() #valor minimo del histograma
        c1=histo[1].max() #valor maximo del histograma

        histo=exposure.histogram(img) #histograma de B

        d=histo[1].min() #valor minimo del histograma
        d1=histo[1].max() #valor maximo del histograma

        # parametros para las metricas de la linealizacion de histograma de acuerdo al tipo de imagen

        #img = exposure.equalize_adapthist(img)

        img = exposure.adjust_log(img,1)

        img[:, :, 0]= exposure.rescale_intensity(img[:, :, 0], in_range=(a, a1)) # 10X=1 / 20X=0.4 / 40x=0.7
        img[:, :, 1]= exposure.rescale_intensity(img[:, :, 1], in_range=(b, b1))   # 10X=2 / 20X=1   / 40x=1.2
        img[:, :, 2]= exposure.rescale_intensity(img[:, :, 2], in_range=(c, c1))   # 10X=4.5 / 20X=12   / 40x=4

        #img= exposure.rescale_intensity(img, in_range=(d, d1))   # 10X=4.5 / 20X=12   / 40x=4

        io.imshow(img) # imprimir imagen
        io.show()
        io.imsave(raiz+'/linealizacion/'+name[i]+'.png',(img)) #guardar la imagen con linealizado
    
def binarizar(humbrales):
    #Binarizacion

    path = raiz+"/linealizacion" #directorio de trabajo local

    name = listar(path) #listar los archivos .png en el directorio
    nuevaruta = r'hem/40x/binario' 
    if not os.path.exists(nuevaruta): os.makedirs(nuevaruta)
         
    thd = humbrales
    
    for i in range(len(name)):

        ihc_rgb = io.imread(path+"/"+name[i]+'.png')
        ihc_hed = rgb2hed(ihc_rgb) #de RGB a HED (hematoxilina, eosina, dao)
        ihc_hed_np = np.array(ihc_hed) # de Numpy a uint8
        
        #th = threshold_otsu(ihc_hed_np[:,:,0])
        
        sol = ihc_hed_np[:,:,0] < thd[i]
        io.imshow(sol)
        io.show()
        
        sol = np.array(np.multiply(sol,255))
        
        io.imsave(raiz+'/binario/'+name[i]+'.png',(np.uint8(sol))) #guardar la imagen con binarizado
 
def vectores():
    #Declaracion de vectores para graficas

    #Nombres

    n=[]

    #Hibridos Totales
    prof=[]
    anoo=[]
    mess=[]
    area=[]
    peri=[]
    ecce=[]
    circ=[]
    eule=[]
    diam=[]
    majo=[]
    mino=[]
    iarea=[]
    iperi=[]
    iecce=[]
    icirc=[]
    ieule=[]
    idiam=[]
    imajo=[]
    imino=[]

    #Puros yaque Totales
    prof1=[]
    anoo1=[]
    mess1=[]
    area1=[]
    peri1=[]
    ecce1=[]
    circ1=[]
    eule1=[]
    diam1=[]
    majo1=[]
    mino1=[]
    iarea1=[]
    iperi1=[]
    iecce1=[]
    icirc1=[]
    ieule1=[]
    idiam1=[]
    imajo1=[]
    imino1=[]
    
    #Puros Rayado Totales
    prof2=[]
    anoo2=[]
    mess2=[]
    area2=[]
    peri2=[]
    ecce2=[]
    circ2=[]
    eule2=[]
    diam2=[]
    majo2=[]
    mino2=[]
    iarea2=[]
    iperi2=[]
    iecce2=[]
    icirc2=[]
    ieule2=[]
    idiam2=[]
    imajo2=[]
    imino2=[]

    #Matriz Hibridos
    mprof=[]
    manoo=[]
    mmess=[]
    marea=[]
    mperi=[]
    mecce=[]
    mcirc=[]
    meule=[]
    mdiam=[]
    mmajo=[]
    mmino=[]
    miarea=[]
    miperi=[]
    miecce=[]
    micirc=[]
    mieule=[]
    midiam=[]
    mimajo=[]
    mimino=[]

    #Matriz Puros Yaque
    mprof1=[]
    manoo1=[]
    mmess1=[]
    marea1=[]
    mperi1=[]
    mecce1=[]
    mcirc1=[]
    meule1=[]
    mdiam1=[]
    mmajo1=[]
    mmino1=[]
    miarea1=[]
    miperi1=[]
    miecce1=[]
    micirc1=[]
    mieule1=[]
    midiam1=[]
    mimajo1=[]
    mimino1=[]

    #Matriz Puros Rayado
    mprof2=[]
    manoo2=[]
    mmess2=[]
    marea2=[]
    mperi2=[]
    mecce2=[]
    mcirc2=[]
    meule2=[]
    mdiam2=[]
    mmajo2=[]
    mmino2=[]
    miarea2=[]
    miperi2=[]
    miecce2=[]
    micirc2=[]
    mieule2=[]
    midiam2=[]
    mimajo2=[]
    mimino2=[]

    #Auxiliares

    profaux=[]
    anooaux=[]
    messaux=[]
    areaaux=[]
    periaux=[]
    ecceaux=[]
    circaux=[]
    euleaux=[]
    diamaux=[]
    majoaux=[]
    minoaux=[]
    iareaaux=[]
    iperiaux=[]
    iecceaux=[]
    icircaux=[]
    ieuleaux=[]
    idiamaux=[]
    imajoaux=[]
    iminoaux=[]

    return n,prof,anoo,mess,area,peri,ecce,circ,eule,diam,majo,mino,iarea,iperi,iecce,icirc,ieule,idiam,imajo,imino,prof1,anoo1,mess1,area1,peri1,ecce1,circ1,eule1,diam1,majo1,mino1,iarea1,iperi1,iecce1,icirc1,ieule1,idiam1,imajo1,imino1,mprof,manoo,mmess,marea,mperi,mecce,mcirc,meule,mdiam,mmajo,mmino,miarea,miperi,miecce,micirc,mieule,midiam,mimajo,mimino,mprof1,manoo1,mmess1,marea1,mperi1,mecce1,mcirc1,meule1,mdiam1,mmajo1,mmino1,miarea1,miperi1,miecce1,micirc1,mieule1,midiam1,mimajo1,mimino1,profaux,anooaux,messaux,areaaux,periaux,ecceaux,circaux,euleaux,diamaux,majoaux,minoaux,iareaaux,iperiaux,iecceaux,icircaux,ieuleaux,idiamaux,imajoaux,iminoaux,mprof2,manoo2,mmess2,marea2,mperi2,mecce2,mcirc2,meule2,mdiam2,mmajo2,mmino2,miarea2,miperi2,miecce2,micirc2,mieule2,midiam2,mimajo2,mimino2,prof2,anoo2,mess2,area2,peri2,ecce2,circ2,eule2,diam2,majo2,mino2,iarea2,iperi2,iecce2,icirc2,ieule2,idiam2,imajo2,imino2

def labeling(n,prof,anoo,mess,area,peri,ecce,circ,eule,diam,majo,mino,iarea,iperi,iecce,icirc,ieule,idiam,imajo,imino,prof1,anoo1,mess1,area1,peri1,ecce1,circ1,eule1,diam1,majo1,mino1,iarea1,iperi1,iecce1,icirc1,ieule1,idiam1,imajo1,imino1,mprof,manoo,mmess,marea,mperi,mecce,mcirc,meule,mdiam,mmajo,mmino,miarea,miperi,miecce,micirc,mieule,midiam,mimajo,mimino,mprof1,manoo1,mmess1,marea1,mperi1,mecce1,mcirc1,meule1,mdiam1,mmajo1,mmino1,miarea1,miperi1,miecce1,micirc1,mieule1,midiam1,mimajo1,mimino1,profaux,anooaux,messaux,areaaux,periaux,ecceaux,circaux,euleaux,diamaux,majoaux,minoaux,iareaaux,iperiaux,iecceaux,icircaux,ieuleaux,idiamaux,imajoaux,iminoaux,mprof2,manoo2,mmess2,marea2,mperi2,mecce2,mcirc2,meule2,mdiam2,mmajo2,mmino2,miarea2,miperi2,miecce2,micirc2,mieule2,midiam2,mimajo2,mimino2,prof2,anoo2,mess2,area2,peri2,ecce2,circ2,eule2,diam2,majo2,mino2,iarea2,iperi2,iecce2,icirc2,ieule2,idiam2,imajo2,imino2):
    #Labeling

    path = raiz+'/binarioM/' #directorio de imagenes binarias
    name = listar(path) #listar los archivos .png en el directorio

    try:
        import cPickle as pickle  
    except ImportError:  
        import pickle 

    f = open(raiz+'/40X Micras.txt','w')
    s = open(raiz+'/40X Pixeles.txt','w')

    f.write('nombre,identificador,tipo,profundidad,mes,año,area,perimeter,eccentricity,circularity,euler_number,equivalent_diameter,major_axis_length,minor_axis_length,min_row,min_col,max_row,max_col,area_in,perimeter_in,eccentricity_in,circularity_in,euler_number_in,equivalent_diameter_in,major_axis_length_in,minor_axis_length_in,min_row_in,min_col_in,max_row_in,max_col_in'+'\n')
    s.write('nombre,identificador,tipo,profundidad,mes,año,area,perimeter,eccentricity,circularity,euler_number,equivalent_diameter,major_axis_length,minor_axis_length,min_row,min_col,max_row,max_col,area_in,perimeter_in,eccentricity_in,circularity_in,euler_number_in,equivalent_diameter_in,major_axis_length_in,minor_axis_length_in,min_row_in,min_col_in,max_row_in,max_col_in'+'\n')

    for i in range(len(name)):

        blobs = io.imread(path+"/"+name[i]+'.png')  #leer Imagen

        all_labels = measure.label(blobs,8,255)
        blobs_labels = measure.label(blobs, background=0)

        io.imshow(all_labels)
        io.show()

        io.imshow(blobs_labels)
        io.show()
        #podemos invertir cada uno de los puntos invirtiendo el all_labels[j] dentro del for para luego sacar la imagen sobrante
        prop=regionprops(all_labels) #lista de objetos cada objeto con n atributos de forma
        prop2=regionprops(blobs_labels)

        for j in range(len(prop)):
            if(name[i][0]=='a'):
                das='Cefalico'
            elif(name[i][0]=='b'):
                das='Medio'
            elif(name[i][0]=='c'):
                das='Caudal'

            if(name[i][2:4]=='16'):
                if(name[i][6]=='3'):
                    sas='Puros Yaque'
                    mes='mayo'
                    ano='2017'
                elif(name[i][6]=='4'):
                    sas='Puros Rayado'
                    mes='mayo'
                    ano='2017'
            elif(name[i][2:4]=='17'):
                sas='Hibridos'
                if(name[i][6]=='3'):
                    mes='marzo'
                    ano='2017'
                elif(name[i][6]=='4'):
                    mes='mayo'
                    ano='2017'

            if(name[i][0]=='h'):
                sas='Hibridos'

                if(name[i][7:9]=='25') or (name[i][7:9]=='27'):
                    mes='enero'
                    ano='2017'
                elif(name[i][7:9]=='28'):
                    mes='septiembre'
                    ano='2016'
                elif((name[i][7:9]=='93') or (name[i][7:9]=='94') or (name[i][5:9]=='1738')):
                    mes='julio'
                    ano='2016'

                if(name[i][19]=='1' or name[i][19]=='4' or name[i][19]=='7'):
                    das='Cefalico'
                elif(name[i][19]=='2' or name[i][19]=='5' or name[i][19]=='8'):
                    das='Medio'
                elif(name[i][19]=='3' or name[i][19]=='6' or name[i][19]=='9'):
                    das='Caudal'  


            cont=0
            if conver2(prop[j].equivalent_diameter,raiz) >= 17.8 and conver2(prop[j].equivalent_diameter,raiz) < 1200:
                mnr,mnc,mxr,mxc = prop[j].bbox
                for k in range(len(prop2)):
                    mnr1,mnc1,mxr1,mxc1 = prop2[k].bbox
                    if mnr<mnr1 and mnc<mnc1 and mxr>mxr1 and mxc>mxc1:
                        f.write(str(name[i]+'.png')+','+str(j)+','+sas+','+das+','+mes+','+ano+','+str(conver1(prop[j].area,raiz))+','+str(conver2(prop[j].perimeter,raiz))+','+str(prop[j].eccentricity)+','+str((prop[j].area*4*math.pi)/(prop[j].perimeter*prop[j].perimeter))+','+str(prop[j].euler_number)+','+str(conver2(prop[j].equivalent_diameter,raiz))+','+str(conver2(prop[j].major_axis_length,raiz))+','+str(conver2(prop[j].minor_axis_length,raiz))+','+str(prop[j].bbox)+','+str(conver1(prop2[k].area,raiz))+','+str(conver2(prop2[k].perimeter,raiz))+','+str(prop2[k].eccentricity)+','+str((prop2[k].area*4*math.pi)/(prop2[k].perimeter*prop2[k].perimeter))+','+str(prop2[k].euler_number)+','+str(conver2(prop2[k].equivalent_diameter,raiz))+','+str(conver2(prop2[k].major_axis_length,raiz))+','+str(conver2(prop2[k].minor_axis_length,raiz))+','+str(prop2[k].bbox)+'\n')
                        s.write(str(name[i]+'.png')+','+str(j)+','+sas+','+das+','+mes+','+ano+','+str(prop[j].area)+','+str(prop[j].perimeter)+','+str(prop[j].eccentricity)+','+str((prop[j].area*4*math.pi)/(prop[j].perimeter*prop[j].perimeter))+','+str(prop[j].euler_number)+','+str(prop[j].equivalent_diameter)+','+str(prop[j].major_axis_length)+','+str(prop[j].minor_axis_length)+','+str(prop[j].bbox)+','+str(prop2[k].area)+','+str(prop2[k].perimeter)+','+str(prop2[k].eccentricity)+','+str((prop2[k].area*4*math.pi)/(prop2[k].perimeter*prop2[k].perimeter))+','+str(prop2[k].euler_number)+','+str(prop2[k].equivalent_diameter)+','+str(prop2[k].major_axis_length)+','+str(prop2[k].minor_axis_length)+','+str(prop2[k].bbox)+'\n')
                        cont=1
                        if(sas=='Puros Yaque'):
                            prof1.append(das)
                            anoo1.append(ano)
                            mess1.append(mes)
                            area1.append(conver1(prop[j].area,raiz))
                            peri1.append(conver2(prop[j].perimeter,raiz))
                            ecce1.append(prop[j].eccentricity)
                            circ1.append(round((prop[j].area*4*math.pi)/(prop[j].perimeter*prop[j].perimeter),10) if prop[j].perimeter!=0 else 0)
                            eule1.append(prop[j].euler_number)
                            diam1.append(conver2(prop[j].equivalent_diameter,raiz))
                            majo1.append(conver2(prop[j].major_axis_length,raiz))
                            mino1.append(conver2(prop[j].minor_axis_length,raiz))
                            iarea1.append(conver1(prop2[k].area,raiz))
                            iperi1.append(conver2(prop2[k].perimeter,raiz))
                            iecce1.append(prop2[k].eccentricity)
                            icirc1.append(round((prop2[k].area*4*math.pi)/(prop2[k].perimeter*prop2[k].perimeter),10) if prop2[k].perimeter!=0 else 0)
                            ieule1.append(prop2[k].euler_number)
                            idiam1.append(conver2(prop2[k].equivalent_diameter,raiz))
                            imajo1.append(conver2(prop2[k].major_axis_length,raiz))
                            imino1.append(conver2(prop2[k].minor_axis_length,raiz))
                        elif(sas=='Puros Rayado'):
                            prof2.append(das)
                            anoo2.append(ano)
                            mess2.append(mes)
                            area2.append(conver1(prop[j].area,raiz))
                            peri2.append(conver2(prop[j].perimeter,raiz))
                            ecce2.append(prop[j].eccentricity)
                            circ2.append(round((prop[j].area*4*math.pi)/(prop[j].perimeter*prop[j].perimeter),10) if prop[j].perimeter!=0 else 0)
                            eule2.append(prop[j].euler_number)
                            diam2.append(conver2(prop[j].equivalent_diameter,raiz))
                            majo2.append(conver2(prop[j].major_axis_length,raiz))
                            mino2.append(conver2(prop[j].minor_axis_length,raiz))
                            iarea2.append(conver1(prop2[k].area,raiz))
                            iperi2.append(conver2(prop2[k].perimeter,raiz))
                            iecce2.append(prop2[k].eccentricity)
                            icirc2.append(round((prop2[k].area*4*math.pi)/(prop2[k].perimeter*prop2[k].perimeter),10) if prop2[k].perimeter!=0 else 0)
                            ieule2.append(prop2[k].euler_number)
                            idiam2.append(conver2(prop2[k].equivalent_diameter,raiz))
                            imajo2.append(conver2(prop2[k].major_axis_length,raiz))
                            imino2.append(conver2(prop2[k].minor_axis_length,raiz))

                        elif(sas=='Hibridos'):
                            prof.append(das)
                            anoo.append(ano)
                            mess.append(mes)
                            area.append(conver1(prop[j].area,raiz))
                            peri.append(conver2(prop[j].perimeter,raiz))
                            ecce.append(prop[j].eccentricity)
                            circ.append(round((prop[j].area*4*math.pi)/(prop[j].perimeter*prop[j].perimeter),10) if prop[j].perimeter!=0 else 0)
                            eule.append(prop[j].euler_number)
                            diam.append(conver2(prop[j].equivalent_diameter,raiz))
                            majo.append(conver2(prop[j].major_axis_length,raiz))
                            mino.append(conver2(prop[j].minor_axis_length,raiz))
                            iarea.append(conver1(prop2[k].area,raiz))
                            iperi.append(conver2(prop2[k].perimeter,raiz))
                            iecce.append(prop2[k].eccentricity)
                            icirc.append(round((prop2[k].area*4*math.pi)/(prop2[k].perimeter*prop2[k].perimeter),10) if prop2[k].perimeter!=0 else 0)
                            ieule.append(prop2[k].euler_number)
                            idiam.append(conver2(prop2[k].equivalent_diameter,raiz))
                            imajo.append(conver2(prop2[k].major_axis_length,raiz))
                            imino.append(conver2(prop2[k].minor_axis_length,raiz))
                        break
                    elif cont==len(prop2):
                        f.write(str(name[i]+'.png')+','+str(j)+','+sas+','+das+','+mes+','+ano+','+str(conver1(prop[j].area,raiz))+','+str(conver2(prop[j].perimeter,raiz))+','+str(prop[j].eccentricity)+','+str((prop[j].area*4*math.pi)/(prop[j].perimeter*prop[j].perimeter))+','+str(prop[j].euler_number)+','+str(conver2(prop[j].equivalent_diameter,raiz))+','+str(conver2(prop[j].major_axis_length,raiz))+','+str(conver2(prop[j].minor_axis_length,raiz))+','+str(prop[j].bbox)+',0,0,0,0,0,0,0,0,0,0,0,0'+'\n')
                        s.write(str(name[i]+'.png')+','+str(j)+','+sas+','+das+','+mes+','+ano+','+str(prop[j].area)+','+str(prop[j].perimeter)+','+str(prop[j].eccentricity)+','+str((prop[j].area*4*math.pi)/(prop[j].perimeter*prop[j].perimeter))+','+str(prop[j].euler_number)+','+str(prop[j].equivalent_diameter)+','+str(prop[j].major_axis_length)+','+str(prop[j].minor_axis_length)+','+str(prop[j].bbox)+',0,0,0,0,0,0,0,0,0,0,0,0'+'\n')
                        if(sas=='Puros Yaque'):
                            prof1.append(das)
                            anoo1.append(ano)
                            mess1.append(mes)
                            area1.append(conver1(prop[j].area,raiz))
                            peri1.append(conver2(prop[j].perimeter,raiz))
                            ecce1.append(prop[j].eccentricity)
                            circ1.append((prop[j].area*4*math.pi)/(prop[j].perimeter*prop[j].perimeter))
                            eule1.append(prop[j].euler_number)
                            diam1.append(conver2(prop[j].equivalent_diameter,raiz))
                            majo1.append(conver2(prop[j].major_axis_length,raiz))
                            mino1.append(conver2(prop[j].minor_axis_length,raiz))
                            iarea1.append(0)
                            iperi1.append(0)
                            iecce1.append(0)
                            icirc1.append(0)
                            ieule1.append(0)
                            idiam1.append(0)
                            imajo1.append(0)
                            imino1.append(0)
                        elif(sas=='Puros Rayado'):
                            prof2.append(das)
                            anoo2.append(ano)
                            mess2.append(mes)
                            area2.append(conver1(prop[j].area,raiz))
                            peri2.append(conver2(prop[j].perimeter,raiz))
                            ecce2.append(prop[j].eccentricity)
                            circ2.append((prop[j].area*4*math.pi)/(prop[j].perimeter*prop[j].perimeter))
                            eule2.append(prop[j].euler_number)
                            diam2.append(conver2(prop[j].equivalent_diameter,raiz))
                            majo2.append(conver2(prop[j].major_axis_length,raiz))
                            mino2.append(conver2(prop[j].minor_axis_length,raiz))
                            iarea2.append(0)
                            iperi2.append(0)
                            iecce2.append(0)
                            icirc2.append(0)
                            ieule2.append(0)
                            idiam2.append(0)
                            imajo2.append(0)
                            imino2.append(0)
                        elif(sas=='Hibridos'):
                            prof.append(das)
                            anoo.append(ano)
                            mess.append(mes)
                            area.append(conver1(prop[j].area,raiz))
                            peri.append(conver2(prop[j].perimeter,raiz))
                            ecce.append(prop[j].eccentricity)
                            circ.append((prop[j].area*4*math.pi)/(prop[j].perimeter*prop[j].perimeter))
                            eule.append(prop[j].euler_number)
                            diam.append(conver2(prop[j].equivalent_diameter,raiz))
                            majo.append(conver2(prop[j].major_axis_length,raiz))
                            mino.append(conver2(prop[j].minor_axis_length,raiz))
                            iarea.append(0)
                            iperi.append(0)
                            iecce.append(0)
                            icirc.append(0)
                            ieule.append(0)
                            idiam.append(0)
                            imajo.append(0)
                            imino.append(0)
                        break
    f.close()
    s.close()

    return n,prof,anoo,mess,area,peri,ecce,circ,eule,diam,majo,mino,iarea,iperi,iecce,icirc,ieule,idiam,imajo,imino,prof1,anoo1,mess1,area1,peri1,ecce1,circ1,eule1,diam1,majo1,mino1,iarea1,iperi1,iecce1,icirc1,ieule1,idiam1,imajo1,imino1,mprof,manoo,mmess,marea,mperi,mecce,mcirc,meule,mdiam,mmajo,mmino,miarea,miperi,miecce,micirc,mieule,midiam,mimajo,mimino,mprof1,manoo1,mmess1,marea1,mperi1,mecce1,mcirc1,meule1,mdiam1,mmajo1,mmino1,miarea1,miperi1,miecce1,micirc1,mieule1,midiam1,mimajo1,mimino1,profaux,anooaux,messaux,areaaux,periaux,ecceaux,circaux,euleaux,diamaux,majoaux,minoaux,iareaaux,iperiaux,iecceaux,icircaux,ieuleaux,idiamaux,imajoaux,iminoaux,mprof2,manoo2,mmess2,marea2,mperi2,mecce2,mcirc2,meule2,mdiam2,mmajo2,mmino2,miarea2,miperi2,miecce2,micirc2,mieule2,midiam2,mimajo2,mimino2,prof2,anoo2,mess2,area2,peri2,ecce2,circ2,eule2,diam2,majo2,mino2,iarea2,iperi2,iecce2,icirc2,ieule2,idiam2,imajo2,imino2

def estraccion(n,prof,anoo,mess,area,peri,ecce,circ,eule,diam,majo,mino,iarea,iperi,iecce,icirc,ieule,idiam,imajo,imino,prof1,anoo1,mess1,area1,peri1,ecce1,circ1,eule1,diam1,majo1,mino1,iarea1,iperi1,iecce1,icirc1,ieule1,idiam1,imajo1,imino1,mprof,manoo,mmess,marea,mperi,mecce,mcirc,meule,mdiam,mmajo,mmino,miarea,miperi,miecce,micirc,mieule,midiam,mimajo,mimino,mprof1,manoo1,mmess1,marea1,mperi1,mecce1,mcirc1,meule1,mdiam1,mmajo1,mmino1,miarea1,miperi1,miecce1,micirc1,mieule1,midiam1,mimajo1,mimino1,profaux,anooaux,messaux,areaaux,periaux,ecceaux,circaux,euleaux,diamaux,majoaux,minoaux,iareaaux,iperiaux,iecceaux,icircaux,ieuleaux,idiamaux,imajoaux,iminoaux,mprof2,manoo2,mmess2,marea2,mperi2,mecce2,mcirc2,meule2,mdiam2,mmajo2,mmino2,miarea2,miperi2,miecce2,micirc2,mieule2,midiam2,mimajo2,mimino2,prof2,anoo2,mess2,area2,peri2,ecce2,circ2,eule2,diam2,majo2,mino2,iarea2,iperi2,iecce2,icirc2,ieule2,idiam2,imajo2,imino2):
    #Separacion de datos en la matriz

    lprof = ['Cefalico','Medio','Caudal']
    lano = ['2016','2016','2017','2017','2017']
    lmes = ['julio','septiembre','enero','marzo','mayo']

    #For para los Puros Yaque

    for b in range(len(lprof)):
        for c in range(len(lano)):
            for j in range(len(prof1)):
                if(prof1[j]==lprof[b] and anoo1[j]==lano[c] and mess1[j]==lmes[c]):
                    profaux.append(prof1[j])
                    anooaux.append(anoo1[j])
                    messaux.append(mess1[j])
                    areaaux.append(area1[j])
                    periaux.append(peri1[j])
                    ecceaux.append(ecce1[j])
                    circaux.append(circ1[j])
                    euleaux.append(eule1[j])
                    diamaux.append(diam1[j])
                    majoaux.append(majo1[j])
                    minoaux.append(mino1[j])
                    iareaaux.append(iarea1[j])
                    iperiaux.append(iperi1[j])
                    iecceaux.append(iecce1[j])
                    icircaux.append(icirc1[j])
                    ieuleaux.append(ieule1[j])
                    idiamaux.append(idiam1[j])
                    imajoaux.append(imajo1[j])
                    iminoaux.append(imino1[j])
            mprof1.append(profaux)
            manoo1.append(anooaux)
            mmess1.append(messaux)
            marea1.append(areaaux)
            mperi1.append(periaux)
            mecce1.append(ecceaux)
            mcirc1.append(circaux)
            meule1.append(euleaux)
            mdiam1.append(diamaux)
            mmajo1.append(majoaux)
            mmino1.append(minoaux)
            miarea1.append(iareaaux)
            miperi1.append(iperiaux)
            miecce1.append(iecceaux)
            micirc1.append(icircaux)
            mieule1.append(ieuleaux)
            midiam1.append(idiamaux)
            mimajo1.append(imajoaux)
            mimino1.append(iminoaux)

            #Matrices en 0

            profaux=[]
            anooaux=[]
            messaux=[]
            areaaux=[]
            periaux=[]
            ecceaux=[]
            circaux=[]
            euleaux=[]
            diamaux=[]
            majoaux=[]
            minoaux=[]
            iareaaux=[]
            iperiaux=[]
            iecceaux=[]
            icircaux=[]
            ieuleaux=[]
            idiamaux=[]
            imajoaux=[]
            iminoaux=[]

    #For para los Puros Rayado

    for b in range(len(lprof)):
        for c in range(len(lano)):
            for j in range(len(prof2)):
                if(prof2[j]==lprof[b] and anoo2[j]==lano[c] and mess2[j]==lmes[c]):
                    profaux.append(prof2[j])
                    anooaux.append(anoo2[j])
                    messaux.append(mess2[j])
                    areaaux.append(area2[j])
                    periaux.append(peri2[j])
                    ecceaux.append(ecce2[j])
                    circaux.append(circ2[j])
                    euleaux.append(eule2[j])
                    diamaux.append(diam2[j])
                    majoaux.append(majo2[j])
                    minoaux.append(mino2[j])
                    iareaaux.append(iarea2[j])
                    iperiaux.append(iperi2[j])
                    iecceaux.append(iecce2[j])
                    icircaux.append(icirc2[j])
                    ieuleaux.append(ieule2[j])
                    idiamaux.append(idiam2[j])
                    imajoaux.append(imajo2[j])
                    iminoaux.append(imino2[j])
            mprof2.append(profaux)
            manoo2.append(anooaux)
            mmess2.append(messaux)
            marea2.append(areaaux)
            mperi2.append(periaux)
            mecce2.append(ecceaux)
            mcirc2.append(circaux)
            meule2.append(euleaux)
            mdiam2.append(diamaux)
            mmajo2.append(majoaux)
            mmino2.append(minoaux)
            miarea2.append(iareaaux)
            miperi2.append(iperiaux)
            miecce2.append(iecceaux)
            micirc2.append(icircaux)
            mieule2.append(ieuleaux)
            midiam2.append(idiamaux)
            mimajo2.append(imajoaux)
            mimino2.append(iminoaux)

            #Matrices en 0

            profaux=[]
            anooaux=[]
            messaux=[]
            areaaux=[]
            periaux=[]
            ecceaux=[]
            circaux=[]
            euleaux=[]
            diamaux=[]
            majoaux=[]
            minoaux=[]
            iareaaux=[]
            iperiaux=[]
            iecceaux=[]
            icircaux=[]
            ieuleaux=[]
            idiamaux=[]
            imajoaux=[]
            iminoaux=[]

    #For para los Hibridos

    for b in range(len(lprof)):
        for c in range(len(lano)):
            for j in range(len(prof)):
                if(prof[j]==lprof[b] and anoo[j]==lano[c] and mess[j]==lmes[c]):
                    profaux.append(prof[j])
                    anooaux.append(anoo[j])
                    messaux.append(mess[j])
                    areaaux.append(area[j])
                    periaux.append(peri[j])
                    ecceaux.append(ecce[j])
                    circaux.append(circ[j])
                    euleaux.append(eule[j])
                    diamaux.append(diam[j])
                    majoaux.append(majo[j])
                    minoaux.append(mino[j])
                    iareaaux.append(iarea[j])
                    iperiaux.append(iperi[j])
                    iecceaux.append(iecce[j])
                    icircaux.append(icirc[j])
                    ieuleaux.append(ieule[j])
                    idiamaux.append(idiam[j])
                    imajoaux.append(imajo[j])
                    iminoaux.append(imino[j])
            mprof.append(profaux)
            manoo.append(anooaux)
            mmess.append(messaux)
            marea.append(areaaux)
            mperi.append(periaux)
            mecce.append(ecceaux)
            mcirc.append(circaux)
            meule.append(euleaux)
            mdiam.append(diamaux)
            mmajo.append(majoaux)
            mmino.append(minoaux)
            miarea.append(iareaaux)
            miperi.append(iperiaux)
            miecce.append(iecceaux)
            micirc.append(icircaux)
            mieule.append(ieuleaux)
            midiam.append(idiamaux)
            mimajo.append(imajoaux)
            mimino.append(iminoaux)

            #Matrices en 0

            profaux=[]
            anooaux=[]
            messaux=[]
            areaaux=[]
            periaux=[]
            ecceaux=[]
            circaux=[]
            euleaux=[]
            diamaux=[]
            majoaux=[]
            minoaux=[]
            iareaaux=[]
            iperiaux=[]
            iecceaux=[]
            icircaux=[]
            ieuleaux=[]
            idiamaux=[]
            imajoaux=[]
            iminoaux=[]
    
    return n,prof,anoo,mess,area,peri,ecce,circ,eule,diam,majo,mino,iarea,iperi,iecce,icirc,ieule,idiam,imajo,imino,prof1,anoo1,mess1,area1,peri1,ecce1,circ1,eule1,diam1,majo1,mino1,iarea1,iperi1,iecce1,icirc1,ieule1,idiam1,imajo1,imino1,mprof,manoo,mmess,marea,mperi,mecce,mcirc,meule,mdiam,mmajo,mmino,miarea,miperi,miecce,micirc,mieule,midiam,mimajo,mimino,mprof1,manoo1,mmess1,marea1,mperi1,mecce1,mcirc1,meule1,mdiam1,mmajo1,mmino1,miarea1,miperi1,miecce1,micirc1,mieule1,midiam1,mimajo1,mimino1,profaux,anooaux,messaux,areaaux,periaux,ecceaux,circaux,euleaux,diamaux,majoaux,minoaux,iareaaux,iperiaux,iecceaux,icircaux,ieuleaux,idiamaux,imajoaux,iminoaux,mprof2,manoo2,mmess2,marea2,mperi2,mecce2,mcirc2,meule2,mdiam2,mmajo2,mmino2,miarea2,miperi2,miecce2,micirc2,mieule2,midiam2,mimajo2,mimino2,prof2,anoo2,mess2,area2,peri2,ecce2,circ2,eule2,diam2,majo2,mino2,iarea2,iperi2,iecce2,icirc2,ieule2,idiam2,imajo2,imino2
            
def estadisticos(n,prof,anoo,mess,area,peri,ecce,circ,eule,diam,majo,mino,iarea,iperi,iecce,icirc,ieule,idiam,imajo,imino,prof1,anoo1,mess1,area1,peri1,ecce1,circ1,eule1,diam1,majo1,mino1,iarea1,iperi1,iecce1,icirc1,ieule1,idiam1,imajo1,imino1,mprof,manoo,mmess,marea,mperi,mecce,mcirc,meule,mdiam,mmajo,mmino,miarea,miperi,miecce,micirc,mieule,midiam,mimajo,mimino,mprof1,manoo1,mmess1,marea1,mperi1,mecce1,mcirc1,meule1,mdiam1,mmajo1,mmino1,miarea1,miperi1,miecce1,micirc1,mieule1,midiam1,mimajo1,mimino1,profaux,anooaux,messaux,areaaux,periaux,ecceaux,circaux,euleaux,diamaux,majoaux,minoaux,iareaaux,iperiaux,iecceaux,icircaux,ieuleaux,idiamaux,imajoaux,iminoaux,mprof2,manoo2,mmess2,marea2,mperi2,mecce2,mcirc2,meule2,mdiam2,mmajo2,mmino2,miarea2,miperi2,miecce2,micirc2,mieule2,midiam2,mimajo2,mimino2,prof2,anoo2,mess2,area2,peri2,ecce2,circ2,eule2,diam2,majo2,mino2,iarea2,iperi2,iecce2,icirc2,ieule2,idiam2,imajo2,imino2):
    #estadisticos

    yaque=[marea1,mperi1,mecce1,mcirc1,meule1,mdiam1,mmajo1,mmino1,miarea1,miperi1,miecce1,micirc1,mieule1,midiam1,mimajo1,mimino1]
    Rayado=[marea2,mperi2,mecce2,mcirc2,meule2,mdiam2,mmajo2,mmino2,miarea2,miperi2,miecce2,micirc2,mieule2,midiam2,mimajo2,mimino2]
    hibri=[marea,mperi,mecce,mcirc,meule,mdiam,mmajo,mmino,miarea,miperi,miecce,micirc,mieule,midiam,mimajo,mimino]
    nomb=['Area','Perimetro','Excentricidad','Circularidad','Numero_de_Euler','Diametro','Longitud_Mayor','Longitud_Menor','Area_Interna','Perimetro_interno','Excentricidad_Interna','Circularidad_interna','Numero_de_Euler_Interno','Diametro_Interno','Longitud_Mayor_Interna','Longitud_Menor_Interna']
    prof=['Cefalico','Medio','Caudal']
    f = open(raiz+'/40X Estadisticos_micras.txt','w')
    f.write('Medida,profundidad,tipo,mes,año,Mediana,Media,Moda,Varianza Poblacional,Error Estandar,Desviacion Estandar,Maximo,Minimo'+'\n')
    
    #Análisis estadistico Puros Yaque
    i=0
    for a in range(len(prof)):
        for b in range(len(yaque)):
            if yaque[b][0+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Yaque,Julio,2016,'+str(statis.mean(yaque[b][0+i]))+','+str(statis.median(yaque[b][0+i]))+','+str([x for x in set(yaque[b][0+i]) if yaque[b][0+i].count(x) > 1][0] if [x for x in set(yaque[b][0+i]) if yaque[b][0+i].count(x) > 1] else 0)+','+str(statis.pvariance(yaque[b][0+i]))+','+str(stats.sem(yaque[b][0+i]))+','+str(statis.pstdev(yaque[b][0+i]))+','+str(max(yaque[b][0+i]))+','+str(min(yaque[b][0+i]))+'\n')
                print('Yaque juli 2016 '+prof[a])
                print("Mediana "+nomb[b]+' '+str(statis.mean(yaque[b][0+i])))
                print("Media "+nomb[b]+' '+str(statis.median(yaque[b][0+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(yaque[b][0+i]) if yaque[b][0+i].count(x) > 1][0] if [x for x in set(yaque[b][0+i]) if yaque[b][0+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(yaque[b][0+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(yaque[b][0+i])))
                print("Desviacion Estandar "+nomb[b]+' '+str(statis.pstdev(yaque[b][0+i])))
                print()

            if yaque[b][1+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Yaque,Septiembre,2016,'+str(statis.mean(yaque[b][1+i]))+','+str(statis.median(yaque[b][1+i]))+','+str([x for x in set(yaque[b][1+i]) if yaque[b][1+i].count(x) > 1][0] if [x for x in set(yaque[b][1+i]) if yaque[b][1+i].count(x) > 1] else 0)+','+str(statis.pvariance(yaque[b][1+i]))+','+str(stats.sem(yaque[b][1+i]))+','+str(statis.pstdev(yaque[b][1+i]))+','+str(max(yaque[b][1+i]))+','+str(min(yaque[b][1+i]))+'\n')
                print('Yaque sept 2016 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(yaque[b][1+i])))
                print("Media "+nomb[b]+' '+str(statis.median(yaque[b][1+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(yaque[b][1+i]) if yaque[b][1+i].count(x) > 1][0] if [x for x in set(yaque[b][1+i]) if yaque[b][1+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(yaque[b][1+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(yaque[b][1+i])))
                print("Desviacion Estandar "+nomb[b]+' '+str(statis.pstdev(yaque[b][1+i])))
                print()

            if yaque[b][2+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Yaque,Enero,2017,'+str(statis.mean(yaque[b][2+i]))+','+str(statis.median(yaque[b][2+i]))+','+str([x for x in set(yaque[b][2+i]) if yaque[b][2+i].count(x) > 1][0] if [x for x in set(yaque[b][2+i]) if yaque[b][2+i].count(x) > 1] else 0)+','+str(statis.pvariance(yaque[b][2+i]))+','+str(stats.sem(yaque[b][2+i]))+','+str(statis.pstdev(yaque[b][2+i]))+','+str(max(yaque[b][2+i]))+','+str(min(yaque[b][2+i]))+'\n')
                print('Yaque ener 2017 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(yaque[b][2+i])))
                print("Media "+nomb[b]+' '+str(statis.median(yaque[b][2+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(yaque[b][2+i]) if yaque[b][2+i].count(x) > 1][0] if [x for x in set(yaque[b][2+i]) if yaque[b][2+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(yaque[b][2+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(yaque[b][2+i])))
                print("Desviacion Estandar "+nomb[b]+' '+str(statis.pstdev(yaque[b][2+i])))
                print()

            if yaque[b][3+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Yaque,Marzo,2017,'+str(statis.mean(yaque[b][3+i]))+','+str(statis.median(yaque[b][3+i]))+','+str([x for x in set(yaque[b][3+i]) if yaque[b][3+i].count(x) > 1][0] if [x for x in set(yaque[b][3+i]) if yaque[b][3+i].count(x) > 1] else 0)+','+str(statis.pvariance(yaque[b][3+i]))+','+str(stats.sem(yaque[b][3+i]))+','+str(statis.pstdev(yaque[b][3+i]))+','+str(max(yaque[b][3+i]))+','+str(min(yaque[b][3+i]))+'\n')
                print('Yaque marz 2017 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(yaque[b][3+i])))
                print("Media "+nomb[b]+' '+str(statis.median(yaque[b][3+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(yaque[b][3+i]) if yaque[b][3+i].count(x) > 1][0] if [x for x in set(yaque[b][3+i]) if yaque[b][3+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(yaque[b][3+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(yaque[b][3+i])))
                print("Desviacion Estandar "+nomb[b]+' '+str(statis.pstdev(yaque[b][3+i])))
                print()

            if yaque[b][4+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Yaque,Mayo,2017,'+str(statis.mean(yaque[b][4+i]))+','+str(statis.median(yaque[b][4+i]))+','+str([x for x in set(yaque[b][4+i]) if yaque[b][4+i].count(x) > 1][0] if [x for x in set(yaque[b][4+i]) if yaque[b][4+i].count(x) > 1] else 0)+','+str(statis.pvariance(yaque[b][4+i]))+','+str(stats.sem(yaque[b][4+i]))+','+str(statis.pstdev(yaque[b][4+i]))+','+str(max(yaque[b][4+i]))+','+str(min(yaque[b][4+i]))+'\n')
                print('Yaque mayo 2017 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(yaque[b][4+i])))
                print("Media "+nomb[b]+' '+str(statis.median(yaque[b][4+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(yaque[b][4+i]) if yaque[b][4+i].count(x) > 1][0] if [x for x in set(yaque[b][4+i]) if yaque[b][4+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(yaque[b][4+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(yaque[b][4+i])))
                print("Desviacion Estandar "+nomb[b]+' '+str(statis.pstdev(yaque[b][4+i])))
                print()

        i=i+5

    #Análisis estadistico Puros Rayado
    i=0
    for a in range(len(prof)):
        for b in range(len(Rayado)):
            if Rayado[b][0+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Rayado,Julio,2016,'+str(statis.mean(Rayado[b][0+i]))+','+str(statis.median(Rayado[b][0+i]))+','+str([x for x in set(Rayado[b][0+i]) if Rayado[b][0+i].count(x) > 1][0] if [x for x in set(Rayado[b][0+i]) if Rayado[b][0+i].count(x) > 1] else 0)+','+str(statis.pvariance(Rayado[b][0+i]))+','+str(stats.sem(Rayado[b][0+i]))+','+str(statis.pstdev(Rayado[b][0+i]))+','+str(max(Rayado[b][0+i]))+','+str(min(Rayado[b][0+i]))+'\n')
                print('Rayado juli 2016 '+prof[a])
                print("Mediana "+nomb[b]+' '+str(statis.mean(Rayado[b][0+i])))
                print("Media "+nomb[b]+' '+str(statis.median(Rayado[b][0+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(Rayado[b][0+i]) if Rayado[b][0+i].count(x) > 1][0] if [x for x in set(Rayado[b][0+i]) if Rayado[b][0+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(Rayado[b][0+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(Rayado[b][0+i])))
                print("Desviacion Estandar "+nomb[b]+' '+str(statis.pstdev(Rayado[b][0+i])))
                print()

            if Rayado[b][1+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Rayado,Septiembre,2016,'+str(statis.mean(Rayado[b][1+i]))+','+str(statis.median(Rayado[b][1+i]))+','+str([x for x in set(Rayado[b][1+i]) if Rayado[b][1+i].count(x) > 1][0] if [x for x in set(Rayado[b][1+i]) if Rayado[b][1+i].count(x) > 1] else 0)+','+str(statis.pvariance(Rayado[b][1+i]))+','+str(stats.sem(Rayado[b][1+i]))+','+str(statis.pstdev(Rayado[b][1+i]))+','+str(max(Rayado[b][1+i]))+','+str(min(Rayado[b][1+i]))+'\n')
                print('Rayado sept 2016 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(Rayado[b][1+i])))
                print("Media "+nomb[b]+' '+str(statis.median(Rayado[b][1+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(Rayado[b][1+i]) if Rayado[b][1+i].count(x) > 1][0] if [x for x in set(Rayado[b][1+i]) if Rayado[b][1+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(Rayado[b][1+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(Rayado[b][1+i])))
                print("Desviacion Estandar "+nomb[b]+' '+str(statis.pstdev(Rayado[b][1+i])))
                print()

            if Rayado[b][2+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Rayado,Enero,2017,'+str(statis.mean(Rayado[b][2+i]))+','+str(statis.median(Rayado[b][2+i]))+','+str([x for x in set(Rayado[b][2+i]) if Rayado[b][2+i].count(x) > 1][0] if [x for x in set(Rayado[b][2+i]) if Rayado[b][2+i].count(x) > 1] else 0)+','+str(statis.pvariance(Rayado[b][2+i]))+','+str(stats.sem(Rayado[b][2+i]))+','+str(statis.pstdev(Rayado[b][2+i]))+','+str(max(Rayado[b][2+i]))+','+str(min(Rayado[b][2+i]))+'\n')
                print('Rayado ener 2017 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(Rayado[b][2+i])))
                print("Media "+nomb[b]+' '+str(statis.median(Rayado[b][2+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(Rayado[b][2+i]) if Rayado[b][2+i].count(x) > 1][0] if [x for x in set(Rayado[b][2+i]) if Rayado[b][2+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(Rayado[b][2+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(Rayado[b][2+i])))
                print("Desviacion Estandar "+nomb[b]+' '+str(statis.pstdev(Rayado[b][2+i])))
                print()

            if Rayado[b][3+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Rayado,Marzo,2017,'+str(statis.mean(Rayado[b][3+i]))+','+str(statis.median(Rayado[b][3+i]))+','+str([x for x in set(Rayado[b][3+i]) if Rayado[b][3+i].count(x) > 1][0] if [x for x in set(Rayado[b][3+i]) if Rayado[b][3+i].count(x) > 1] else 0)+','+str(statis.pvariance(Rayado[b][3+i]))+','+str(stats.sem(Rayado[b][3+i]))+','+str(statis.pstdev(Rayado[b][3+i]))+','+str(max(Rayado[b][3+i]))+','+str(min(Rayado[b][3+i]))+'\n')
                print('Rayado marz 2017 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(Rayado[b][3+i])))
                print("Media "+nomb[b]+' '+str(statis.median(Rayado[b][3+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(Rayado[b][3+i]) if Rayado[b][3+i].count(x) > 1][0] if [x for x in set(Rayado[b][3+i]) if Rayado[b][3+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(Rayado[b][3+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(Rayado[b][3+i])))
                print("Desviacion Estandar "+nomb[b]+' '+str(statis.pstdev(Rayado[b][3+i])))
                print()

            if Rayado[b][4+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Rayado,Mayo,2017,'+str(statis.mean(Rayado[b][4+i]))+','+str(statis.median(Rayado[b][4+i]))+','+str([x for x in set(Rayado[b][4+i]) if Rayado[b][4+i].count(x) > 1][0] if [x for x in set(Rayado[b][4+i]) if Rayado[b][4+i].count(x) > 1] else 0)+','+str(statis.pvariance(Rayado[b][4+i]))+','+str(stats.sem(Rayado[b][4+i]))+','+str(statis.pstdev(Rayado[b][4+i]))+','+str(max(Rayado[b][4+i]))+','+str(min(Rayado[b][4+i]))+'\n')
                print('Rayado mayo 2017 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(Rayado[b][4+i])))
                print("Media "+nomb[b]+' '+str(statis.median(Rayado[b][4+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(Rayado[b][4+i]) if Rayado[b][4+i].count(x) > 1][0] if [x for x in set(Rayado[b][4+i]) if Rayado[b][4+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(Rayado[b][4+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(Rayado[b][4+i])))
                print("Desviacion Estandar "+nomb[b]+' '+str(statis.pstdev(Rayado[b][4+i])))
                print()

        i=i+5

    #Análisis estadistico Hibridos
    i=0
    for a in range(len(prof)):
        for b in range(len(hibri)):
            if hibri[b][0+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Hibrido,Julio,2016,'+str(statis.mean(hibri[b][0+i]))+','+str(statis.median(hibri[b][0+i]))+','+str([x for x in set(hibri[b][0+i]) if hibri[b][0+i].count(x) > 1][0] if [x for x in set(hibri[b][0+i]) if hibri[b][0+i].count(x) > 1] else 0)+','+str(statis.pvariance(hibri[b][0+i]))+','+str(stats.sem(hibri[b][0+i]))+','+str(statis.pstdev(hibri[b][0+i]))+','+str(max(hibri[b][0+i]))+','+str(min(hibri[b][0+i]))+'\n')
                print('Hibrido juli 2016 '+prof[a])
                print("Mediana "+nomb[b]+' '+str(statis.mean(hibri[b][0+i])))
                print("Media "+nomb[b]+' '+str(statis.median(hibri[b][0+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(hibri[b][0+i]) if hibri[b][0+i].count(x) > 1][0] if [x for x in set(hibri[b][0+i]) if hibri[b][0+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(hibri[b][0+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(Rayado[b][0+i])))
                print("Desviacion Estandar "+nomb[b]+' '+str(statis.pstdev(hibri[b][0+i])))
                print()

            if hibri[b][1+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Hibrido,Septiembre,2016,'+str(statis.mean(hibri[b][1+i]))+','+str(statis.median(hibri[b][1+i]))+','+str([x for x in set(hibri[b][1+i]) if hibri[b][1+i].count(x) > 1][0] if [x for x in set(hibri[b][1+i]) if hibri[b][1+i].count(x) > 1] else 0)+','+str(statis.pvariance(hibri[b][1+i]))+','+str(stats.sem(hibri[b][1+i]))+','+str(statis.pstdev(hibri[b][1+i]))+','+str(max(hibri[b][1+i]))+','+str(min(hibri[b][1+i]))+'\n')
                print('Hibrido sept 2016 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(hibri[b][1+i])))
                print("Media "+nomb[b]+' '+str(statis.median(hibri[b][1+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(hibri[b][1+i]) if hibri[b][1+i].count(x) > 1][0] if [x for x in set(hibri[b][1+i]) if hibri[b][1+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(hibri[b][1+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(Rayado[b][1+i])))
                print("Desviacion Estandar "+nomb[b]+' '+str(statis.pstdev(hibri[b][1+i])))
                print()

            if hibri[b][2+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Hibrido,Enero,2017,'+str(statis.mean(hibri[b][2+i]))+','+str(statis.median(hibri[b][2+i]))+','+str([x for x in set(hibri[b][2+i]) if hibri[b][2+i].count(x) > 1][0] if [x for x in set(hibri[b][2+i]) if hibri[b][2+i].count(x) > 1] else 0)+','+str(statis.pvariance(hibri[b][2+i]))+','+str(stats.sem(hibri[b][2+i]))+','+str(statis.pstdev(hibri[b][2+i]))+','+str(max(hibri[b][2+i]))+','+str(min(hibri[b][2+i]))+'\n')
                print('Hibrido ener 2017 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(hibri[b][2+i])))
                print("Media "+nomb[b]+' '+str(statis.median(hibri[b][2+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(hibri[b][2+i]) if hibri[b][2+i].count(x) > 1][0] if [x for x in set(hibri[b][2+i]) if hibri[b][2+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(hibri[b][2+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(Rayado[b][2+i])))
                print("Desviacion Estandar "+nomb[b]+' '+str(statis.pstdev(hibri[b][2+i])))
                print()

            if hibri[b][3+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Hibrido,Marzo,2017,'+str(statis.mean(hibri[b][3+i]))+','+str(statis.median(hibri[b][3+i]))+','+str([x for x in set(hibri[b][3+i]) if hibri[b][3+i].count(x) > 1][0] if [x for x in set(hibri[b][3+i]) if hibri[b][3+i].count(x) > 1] else 0)+','+str(statis.pvariance(hibri[b][3+i]))+','+str(stats.sem(hibri[b][3+i]))+','+str(statis.pstdev(hibri[b][3+i]))+','+str(max(hibri[b][3+i]))+','+str(min(hibri[b][3+i]))+'\n')
                print('Hibrido marz 2017 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(hibri[b][3+i])))
                print("Media "+nomb[b]+' '+str(statis.median(hibri[b][3+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(hibri[b][3+i]) if hibri[b][3+i].count(x) > 1][0] if [x for x in set(hibri[b][3+i]) if hibri[b][3+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(hibri[b][3+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(Rayado[b][3+i])))
                print("Desviacion Estandar "+nomb[b]+' '+str(statis.pstdev(hibri[b][3+i])))
                print()

            if hibri[b][4+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Hibrido,Mayo,2017,'+str(statis.mean(hibri[b][4+i]))+','+str(statis.median(hibri[b][4+i]))+','+str([x for x in set(hibri[b][4+i]) if hibri[b][4+i].count(x) > 1][0] if [x for x in set(hibri[b][4+i]) if hibri[b][4+i].count(x) > 1] else 0)+','+str(statis.pvariance(hibri[b][4+i]))+','+str(stats.sem(hibri[b][4+i]))+','+str(statis.pstdev(hibri[b][4+i]))+','+str(max(hibri[b][4+i]))+','+str(min(hibri[b][4+i]))+'\n')
                print('Hibrido mayo 2017 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(hibri[b][4+i])))
                print("Media "+nomb[b]+' '+str(statis.median(hibri[b][4+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(hibri[b][4+i]) if hibri[b][4+i].count(x) > 1][0] if [x for x in set(hibri[b][4+i]) if hibri[b][4+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(hibri[b][4+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(Rayado[b][4+i])))
                print("Desviacion Estandar "+nomb[b]+' '+str(statis.pstdev(hibri[b][4+i])))
                print()

        i=i+5

    f.close()
    return yaque,Rayado,hibri,nomb,n,prof,anoo,mess,area,peri,ecce,circ,eule,diam,majo,mino,iarea,iperi,iecce,icirc,ieule,idiam,imajo,imino,prof1,anoo1,mess1,area1,peri1,ecce1,circ1,eule1,diam1,majo1,mino1,iarea1,iperi1,iecce1,icirc1,ieule1,idiam1,imajo1,imino1,mprof,manoo,mmess,marea,mperi,mecce,mcirc,meule,mdiam,mmajo,mmino,miarea,miperi,miecce,micirc,mieule,midiam,mimajo,mimino,mprof1,manoo1,mmess1,marea1,mperi1,mecce1,mcirc1,meule1,mdiam1,mmajo1,mmino1,miarea1,miperi1,miecce1,micirc1,mieule1,midiam1,mimajo1,mimino1,profaux,anooaux,messaux,areaaux,periaux,ecceaux,circaux,euleaux,diamaux,majoaux,minoaux,iareaaux,iperiaux,iecceaux,icircaux,ieuleaux,idiamaux,imajoaux,iminoaux,mprof2,manoo2,mmess2,marea2,mperi2,mecce2,mcirc2,meule2,mdiam2,mmajo2,mmino2,miarea2,miperi2,miecce2,micirc2,mieule2,midiam2,mimajo2,mimino2,prof2,anoo2,mess2,area2,peri2,ecce2,circ2,eule2,diam2,majo2,mino2,iarea2,iperi2,iecce2,icirc2,ieule2,idiam2,imajo2,imino2
    
def histogramas(yaque,Rayado,hibri,nomb,n,prof,anoo,mess,area,peri,ecce,circ,eule,diam,majo,mino,iarea,iperi,iecce,icirc,ieule,idiam,imajo,imino,prof1,anoo1,mess1,area1,peri1,ecce1,circ1,eule1,diam1,majo1,mino1,iarea1,iperi1,iecce1,icirc1,ieule1,idiam1,imajo1,imino1,mprof,manoo,mmess,marea,mperi,mecce,mcirc,meule,mdiam,mmajo,mmino,miarea,miperi,miecce,micirc,mieule,midiam,mimajo,mimino,mprof1,manoo1,mmess1,marea1,mperi1,mecce1,mcirc1,meule1,mdiam1,mmajo1,mmino1,miarea1,miperi1,miecce1,micirc1,mieule1,midiam1,mimajo1,mimino1,profaux,anooaux,messaux,areaaux,periaux,ecceaux,circaux,euleaux,diamaux,majoaux,minoaux,iareaaux,iperiaux,iecceaux,icircaux,ieuleaux,idiamaux,imajoaux,iminoaux,mprof2,manoo2,mmess2,marea2,mperi2,mecce2,mcirc2,meule2,mdiam2,mmajo2,mmino2,miarea2,miperi2,miecce2,micirc2,mieule2,midiam2,mimajo2,mimino2,prof2,anoo2,mess2,area2,peri2,ecce2,circ2,eule2,diam2,majo2,mino2,iarea2,iperi2,iecce2,icirc2,ieule2,idiam2,imajo2,imino2):
    #histogramas Externos

    plt.hist([area1,area2,area], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Hembras Área")
    plt.savefig(raiz+" area-hem.png",bbox_inches='tight')
    plt.show()

    plt.hist([peri1,peri2,peri], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Hembras Perimetro")
    plt.savefig(raiz+" perimetro-hem.png",bbox_inches='tight')
    plt.show()

    plt.hist([ecce1,ecce2,ecce], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Hembras Excentricidad")
    plt.savefig(raiz+" excentricidad-hem.png",bbox_inches='tight')
    plt.show()

    plt.hist([circ1,circ2,circ], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Hembras Circularidad")
    plt.savefig(raiz+" circularidad-hem.png",bbox_inches='tight')
    plt.show()

    plt.hist([eule1,eule2,eule], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Hembras Numero de Euler")
    plt.savefig(raiz+" numero_euler-hem.png",bbox_inches='tight')
    plt.show()

    plt.hist([diam1,diam2,diam], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Hembras Diametro")
    plt.savefig(raiz+" Diametro-hem.png",bbox_inches='tight')
    plt.show()

    plt.hist([majo1,majo2,majo], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Hembras Longitud del eje mayor")
    plt.savefig(raiz+" Longitud_mayor-hem.png",bbox_inches='tight')
    plt.show()

    plt.hist([mino1,mino2,mino], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Hembras Longitud del eje menor")
    plt.savefig(raiz+" Longitud_menor-hem.png",bbox_inches='tight')
    plt.show()
    
    #histogramas Internos

    plt.hist([iarea1,iarea2,iarea], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Hembras Área Interna")
    plt.savefig(raiz+" area_interna-hem.png",bbox_inches='tight')
    plt.show()

    plt.hist([iperi1,iperi2,iperi], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Hembras Perimetro Interno")
    plt.savefig(raiz+" perimetro_interno-hem.png",bbox_inches='tight')
    plt.show()

    plt.hist([iecce1,iecce2,iecce], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Hembras Excentricidad Interna")
    plt.savefig(raiz+" excentricidad_interna-hem.png",bbox_inches='tight')
    plt.show()

    plt.hist([circ1,circ2,circ], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Hembras Circularidad Interna")
    plt.savefig(raiz+" circularidad-interno-hem.png",bbox_inches='tight')
    plt.show()

    plt.hist([ieule1,ieule2,ieule], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Hembras Numero de Euler Interno")
    plt.savefig(raiz+" numero_euler_interno-hem.png",bbox_inches='tight')
    plt.show()

    plt.hist([idiam1,idiam2,idiam], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Hembras Diametro Interno")
    plt.savefig(raiz+" Diametro_interno-hem.png",bbox_inches='tight')
    plt.show()

    plt.hist([imajo1,imajo2,imajo], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Hembras Longitud del eje mayor Interno")
    plt.savefig(raiz+" Longitud_mayor_interna-hem.png",bbox_inches='tight')
    plt.show()

    plt.hist([imino1,imino2,imino], range=[0, 45], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Hembras Longitud del eje menor Interno")
    plt.savefig(raiz+" Longitud_menor_interna-hem.png",bbox_inches='tight')
    plt.show()

def cajas(yaque,Rayado,hibri,nomb):
    #Cajas Puros yaque Cortes

    #Cajas yaque Cortes

    for b in range(len(yaque)):

        if b in [0,8]:
            etiq = u'Tamaño (µm ^ 2)'
        elif b in [1,5,6,7,9,13,14,15]:
            etiq = u'Tamaño (µm)'
        else:
            etiq = ''

        if(yaque[b][0]!=[] or yaque[b][5]!=[] or yaque[b][10]!=[]):
            plt.ion()
            plt.boxplot([yaque[b][0],yaque[b][5],yaque[b][10]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Yaque Hembras Julio 2016')
            plt.savefig(raiz+nomb[b]+"-Yaque-Hem-Julio.png",bbox_inches='tight')
            plt.show()

        if(yaque[b][1]!=[] or yaque[b][6]!=[] or yaque[b][11]!=[]):
            plt.ion()
            plt.boxplot([yaque[b][1],yaque[b][6],yaque[b][11]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Yaque Hembras Septiembre 2016')
            plt.savefig(raiz+nomb[b]+"-Yaque-Hem-Septiembre.png",bbox_inches='tight')
            plt.show()

        if(yaque[b][2]!=[] or yaque[b][7]!=[] or yaque[b][12]!=[]):
            plt.ion()
            plt.boxplot([yaque[b][2],yaque[b][7],yaque[b][12]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Yaque Hembras Enero 2017')
            plt.savefig(raiz+nomb[b]+"-Yaque-Hem-Enero.png",bbox_inches='tight')
            plt.show()

        if(yaque[b][3]!=[] or yaque[b][8]!=[] or yaque[b][13]!=[]):
            plt.ion()
            plt.boxplot([yaque[b][3],yaque[b][8],yaque[b][13]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Yaque Hembras Marzo 2017')
            plt.savefig(raiz+nomb[b]+"-Yaque-Hem-Marzo.png",bbox_inches='tight')
            plt.show()

        if(yaque[b][4]!=[] or yaque[b][9]!=[] or yaque[b][14]!=[]):
            plt.ion()
            plt.boxplot([yaque[b][4],yaque[b][9],yaque[b][14]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Yaque Hembras Mayo 2017')
            plt.savefig(raiz+nomb[b]+"-Yaque-Hem-Mayo.png",bbox_inches='tight')
            plt.show()

    #Cajas Rayado Cortes

    for b in range(len(Rayado)):
        
        if b in [0,8]:
            etiq = u'Tamaño (µm ^ 2)'
        elif b in [1,5,6,7,9,13,14,15]:
            etiq = u'Tamaño (µm)'
        else:
            etiq = ''

        if(Rayado[b][0]!=[] or Rayado[b][5]!=[] or Rayado[b][10]!=[]):
            plt.ion()
            plt.boxplot([Rayado[b][0],Rayado[b][5],Rayado[b][10]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Rayado Hembras Julio 2016')
            plt.savefig(raiz+nomb[b]+"-Rayado-Hem-Julio.png",bbox_inches='tight')
            plt.show()

        if(Rayado[b][1]!=[] or Rayado[b][6]!=[] or Rayado[b][11]!=[]):
            plt.ion()
            plt.boxplot([Rayado[b][1],Rayado[b][6],Rayado[b][11]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Rayado Hembras Septiembre 2016')
            plt.savefig(raiz+nomb[b]+"-Rayado-Hem-Septiembre.png",bbox_inches='tight')
            plt.show()

        if(Rayado[b][2]!=[] or Rayado[b][7]!=[] or Rayado[b][12]!=[]):
            plt.ion()
            plt.boxplot([Rayado[b][2],Rayado[b][7],Rayado[b][12]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Rayado Hembras Enero 2017')
            plt.savefig(raiz+nomb[b]+"-Rayado-Hem-Enero.png",bbox_inches='tight')
            plt.show()

        if(Rayado[b][3]!=[] or Rayado[b][8]!=[] or Rayado[b][13]!=[]):
            plt.ion()
            plt.boxplot([Rayado[b][3],Rayado[b][8],Rayado[b][13]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Rayado Hembras Marzo 2017')
            plt.savefig(raiz+nomb[b]+"-Rayado-Hem-Marzo.png",bbox_inches='tight')
            plt.show()

        if(Rayado[b][4]!=[] or Rayado[b][9]!=[] or Rayado[b][14]!=[]):
            plt.ion()
            plt.boxplot([Rayado[b][4],Rayado[b][9],Rayado[b][14]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Rayado Hembras Mayo 2017')
            plt.savefig(raiz+nomb[b]+"-Rayado-Hem-Mayo.png",bbox_inches='tight')
            plt.show()
            
    #Cajas Hibridos Cortes
    
    for b in range(len(hibri)):

        if b in [0,8]:
            etiq = u'Tamaño (µm ^ 2)'
        elif b in [1,5,6,7,9,13,14,15]:
            etiq = u'Tamaño (µm)'
        else:
            etiq = ''

        if(hibri[b][0]!=[] or hibri[b][5]!=[] or hibri[b][10]!=[]):
            plt.ion()
            plt.boxplot([hibri[b][0],hibri[b][5],hibri[b][10]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Hibridos Hembras Julio 2016')
            plt.savefig(raiz+nomb[b]+"-Hibridos-Hem-Julio.png",bbox_inches='tight')
            plt.show()

        if(hibri[b][1]!=[] or hibri[b][6]!=[] or hibri[b][11]!=[]):
            plt.ion()
            plt.boxplot([hibri[b][1],hibri[b][6],hibri[b][11]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Hibridos Hembras Septiembre 2016')
            plt.savefig(raiz+nomb[b]+"-Hibridos-Hem-Septiembre.png",bbox_inches='tight')
            plt.show()

        if(hibri[b][2]!=[] or hibri[b][7]!=[] or hibri[b][12]!=[]):
            plt.ion()
            plt.boxplot([hibri[b][2],hibri[b][7],hibri[b][12]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Hibridos Hembras Enero 2017')
            plt.savefig(raiz+nomb[b]+"-Hibridos-Hem-Enero.png",bbox_inches='tight')
            plt.show()

        if(hibri[b][3]!=[] or hibri[b][8]!=[] or hibri[b][13]!=[]):
            plt.ion()
            plt.boxplot([hibri[b][3],hibri[b][8],hibri[b][13]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Hibridos Hembras Marzo 2017')
            plt.savefig(raiz+nomb[b]+"-Hibridos-Hem-Marzo.png",bbox_inches='tight')
            plt.show()

        if(hibri[b][4]!=[] or hibri[b][9]!=[] or hibri[b][14]!=[]):
            plt.ion()
            plt.boxplot([hibri[b][4],hibri[b][9],hibri[b][14]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Hibridos Hembras Mayo 2017')
            plt.savefig(raiz+nomb[b]+"-Hibridos-Hem-Mayo.png",bbox_inches='tight')
            plt.show()

    for b in range(len(hibri)):
        
        if b in [0,8]:
            etiq = u'Tamaño (µm ^ 2)'
        elif b in [1,5,6,7,9,13,14,15]:
            etiq = u'Tamaño (µm)'
        else:
            etiq = ''
        
        plt.ion()
        plt.boxplot([hibri[b][0]+hibri[b][5]+hibri[b][10],hibri[b][1]+hibri[b][6]+hibri[b][11],hibri[b][2]+hibri[b][7]+hibri[b][12],hibri[b][3]+hibri[b][8]+hibri[b][13],hibri[b][4]+hibri[b][9]+hibri[b][14]])
        plt.xticks([1,2,3,4,5], ['jul 2016', 'sep 2016','ene 2017', 'mar 2017', 'may 2017'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
        plt.ylabel(etiq)
        plt.title(nomb[b] +' hibridos Hembras')
        plt.savefig(raiz+nomb[b]+"-Hibridos-Cajas-Hem.png",bbox_inches='tight')
        plt.show()
     
def diagrama_medias(yaque,Rayado,hibri,nomb):
    
    #Diagrama medias Puros yaque Cortes
    x_aux=[]
    y_aux=[]
    yerr_aux=[]
    lista=[]

    lista1=[]
    lista2=[]
    lista3=[]

    x=[]
    y=[]
    yerr=[]

    for b in range(len(yaque)):
        if(yaque[b][0]!=[] or yaque[b][5]!=[] or yaque[b][10]!=[]):
            lista+=yaque[b][0]
            lista+=yaque[b][5]
            lista+=yaque[b][10]
            x_aux.append('Jul 2016')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(yaque[b][1]!=[] or yaque[b][6]!=[] or yaque[b][11]!=[]):
            lista+=yaque[b][1]
            lista+=yaque[b][6]
            lista+=yaque[b][11]
            x_aux.append('Sep 2016')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(yaque[b][2]!=[] or yaque[b][7]!=[] or yaque[b][12]!=[]):
            lista+=yaque[b][2]
            lista+=yaque[b][7]
            lista+=yaque[b][12]
            x_aux.append('Ene 2017')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(yaque[b][3]!=[] or yaque[b][8]!=[] or yaque[b][13]!=[]):
            lista+=yaque[b][3]
            lista+=yaque[b][8]
            lista+=yaque[b][13]
            x_aux.append('Mar 2017')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(yaque[b][4]!=[] or yaque[b][9]!=[] or yaque[b][14]!=[]):
            lista+=yaque[b][4]
            lista+=yaque[b][9]
            lista+=yaque[b][14]
            x_aux.append('May 2017')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        x.append(x_aux)
        y.append(y_aux)
        yerr.append(yerr_aux)
        x_aux=[]
        y_aux=[]
        yerr_aux=[]

    #Diagrama medias Puros Rayado Cortes

    x1=[]
    y1=[]
    yerr1=[]

    for b in range(len(Rayado)):
        if(Rayado[b][0]!=[] or Rayado[b][5]!=[] or Rayado[b][10]!=[]):
            lista+=Rayado[b][0]
            lista+=Rayado[b][5]
            lista+=Rayado[b][10]
            x_aux.append('Jul 2016')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(Rayado[b][1]!=[] or Rayado[b][6]!=[] or Rayado[b][11]!=[]):
            lista+=Rayado[b][1]
            lista+=Rayado[b][6]
            lista+=Rayado[b][11]
            x_aux.append('Sep 2016')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(Rayado[b][2]!=[] or Rayado[b][7]!=[] or Rayado[b][12]!=[]):
            lista+=Rayado[b][2]
            lista+=Rayado[b][7]
            lista+=Rayado[b][12]
            x_aux.append('Ene 2017')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(Rayado[b][3]!=[] or Rayado[b][8]!=[] or Rayado[b][13]!=[]):
            lista+=Rayado[b][3]
            lista+=Rayado[b][8]
            lista+=Rayado[b][13]
            x_aux.append('Mar 2017')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(Rayado[b][4]!=[] or Rayado[b][9]!=[] or Rayado[b][14]!=[]):
            lista+=Rayado[b][4]
            lista+=Rayado[b][9]
            lista+=Rayado[b][14]
            x_aux.append('May 2017')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        x1.append(x_aux)
        y1.append(y_aux)
        yerr1.append(yerr_aux)
        x_aux=[]
        y_aux=[]
        yerr_aux=[]

    #Diagrama medias Hibridos Cortes

    x2=[]
    y2=[]
    yerr2=[]

    for b in range(len(hibri)):
        if(hibri[b][0]!=[] or hibri[b][5]!=[] or hibri[b][10]!=[]):
            lista+=hibri[b][0]
            lista+=hibri[b][5]
            lista+=hibri[b][10]
            x_aux.append('Jul 2016')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(hibri[b][1]!=[] or hibri[b][6]!=[] or hibri[b][11]!=[]):
            lista+=hibri[b][1]
            lista+=hibri[b][6]
            lista+=hibri[b][11]
            x_aux.append('Sep 2016')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(hibri[b][2]!=[] or hibri[b][7]!=[] or hibri[b][12]!=[]):
            lista+=hibri[b][2]
            lista+=hibri[b][7]
            lista+=hibri[b][12]
            x_aux.append('Ene 2016')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(hibri[b][3]!=[] or hibri[b][8]!=[] or hibri[b][13]!=[]):
            lista+=hibri[b][3]
            lista+=hibri[b][8]
            lista+=hibri[b][13]
            x_aux.append('Mar 2017')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(hibri[b][4]!=[] or hibri[b][9]!=[] or hibri[b][14]!=[]):
            lista+=hibri[b][4]
            lista+=hibri[b][9]
            lista+=hibri[b][14]
            x_aux.append('May 2017')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        x2.append(x_aux)
        y2.append(y_aux)
        yerr2.append(yerr_aux)
        x_aux=[]
        y_aux=[]
        yerr_aux=[]

    for b in range(len(nomb)):

        if b in [0,8]:
            etiq = u'Tamaño (µm ^ 2)'
        elif b in [1,5,6,7,9,13,14,15]:
            etiq = u'Tamaño (µm)'
        else:
            etiq = ''

        plt.errorbar(x[b], y[b], yerr[b], marker='s', ecolor='green', mfc='black',mec='green')
        plt.title(nomb[b]+' Yaque Hembras')
        plt.ylabel(etiq)
        plt.savefig(raiz+nomb[b]+"-yaque-hem.png",bbox_inches='tight')
        plt.show()
        plt.errorbar(x1[b], y1[b], yerr1[b], marker='s', ecolor='blue', mfc='violet',mec='blue')
        plt.title(nomb[b]+' Rayado Hembras')
        plt.ylabel(etiq)
        plt.savefig(raiz+nomb[b]+"-Rayado-hem.png",bbox_inches='tight')
        plt.show()
        plt.errorbar(x2[b], y2[b], yerr2[b], marker='s', ecolor='red', mfc='pink',mec='red')
        plt.title(nomb[b]+' Hibridos Hembras')
        plt.ylabel(etiq)
        plt.savefig(raiz+nomb[b]+"-hibridos-hem.png",bbox_inches='tight')
        plt.show()

    xt=[]
    yt=[]
    yerrt=[]
    for b in range(len(nomb)):
        if(yaque[b][4]!=[] or yaque[b][9]!=[] or yaque[b][14]!=[]):
            lista+=yaque[b][4]
            lista+=yaque[b][9]
            lista+=yaque[b][14]
            x_aux.append('Yaque')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista1=lista
            lista=[]

        if(Rayado[b][4]!=[] or Rayado[b][9]!=[] or Rayado[b][14]!=[]):
            lista+=Rayado[b][4]
            lista+=Rayado[b][9]
            lista+=Rayado[b][14]
            x_aux.append('Rayado')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista2=lista
            lista=[]
        
        if(hibri[b][4]!=[] or hibri[b][9]!=[] or hibri[b][14]!=[]):
            lista+=hibri[b][4]
            lista+=hibri[b][9]
            lista+=hibri[b][14]
            x_aux.append('Hibridos')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista3=lista
            lista=[]
    
        plt.hist([lista1,lista2,lista3], label=['Yaque','Rayado','Hibridos'])
        plt.legend(loc='upper right')
        plt.title(nomb[b]+' Histograma Hembras Mayo 2017')
        plt.savefig(raiz+"Mac-histo-"+nomb[b]+".png",bbox_inches='tight')
        plt.show()

        lista1=[]
        lista2=[]
        lista3=[]
    
        xt.append(x_aux)
        yt.append(y_aux)
        yerrt.append(yerr_aux)
        x_aux=[]
        y_aux=[]
        yerr_aux=[]

    for b in range(len(nomb)):

        if b in [0,8]:
            etiq = u'Tamaño (µm ^ 2)'
        elif b in [1,5,6,7,9,13,14,15]:
            etiq = u'Tamaño (µm)'
        else:
            etiq = ''

        plt.errorbar(xt[b], yt[b], yerrt[b], marker='s', ecolor='green', mfc='black',mec='green')
        plt.title(nomb[b]+' Medias Hembras Mayo 2017')
        plt.ylabel(etiq)
        plt.savefig(raiz+nomb[b]+"-medias-mac.png",bbox_inches='tight')
        plt.show()

class MiClase:
    def __init__(self):
        print("una clase")
