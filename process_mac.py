﻿#Importar las librerias

import numpy as np
import os
import shutil
import matplotlib.pyplot as plt 
import csv
import statistics as statis
import math

from matplotlib.pyplot import hist, show
from matplotlib.colors import LinearSegmentedColormap
from scipy import ndimage, stats
from skimage.filters import threshold_otsu, threshold_yen
from skimage.color import rgb2hed
from skimage.measure import regionprops
from skimage.feature import peak_local_max
from skimage import morphology
from skimage import data, exposure, img_as_float
from skimage import io, color, filters
from skimage import util
from skimage import measure
from skimage.morphology import watershed, black_tophat
from statistics import mode

raiz='mac/40X' #directorio raiz

def listar(de):
    #Variable para la ruta al directorio
    path = de
 
    #Lista vacia para incluir los ficheros
    name = []
 
    #Lista con todos los ficheros del directorio:
    lstDir = os.walk(path)   #os.walk() Lista directorios y ficheros
    #Crea una lista de los ficheros png que existen en el directorio y los incluye a la lista.
    i=0
    for root, dirs, files in lstDir: # recorre el direcctorio sacando los datos de las carpetas
        for fichero in files:
            (nombreFichero, extension) = os.path.splitext(fichero)
    
            if(extension == ".png"):
                name.append(nombreFichero)
        i += 1
        if i >= 1:
            break # rompe el ciclo para poder sacar solamente lo de la carpeta principal
    return name

def conver1(val, raiz):
    sol=0
    if(raiz=='mac/20X'):
        sol=(2720.6656*val)/303623.0404
    if(raiz=='mac/40X'):
        sol=(708.0921*val)/303623.0404
    return sol

def conver2(val, raiz):
    sol=0
    if(raiz=='mac/20X'):
        sol=(52.16*val)/551.02
    if(raiz=='mac/40X'):
        sol=(26.61*val)/551.02
    return sol

#linealizacion
def linealizar():
    path=raiz#directorio de trabajo
    name = listar(raiz) # entra a la funcion para sacar los archivos .png
    nuevaruta = r'mac/40X/linealizacion' #Ruta directorio
    if not os.path.exists(nuevaruta): os.makedirs(nuevaruta) #Validar y crear directorio

    for i in range(len(name)):
        img = io.imread(raiz+"/"+name[i]+'.png')  #leer Imagen 

        histo=exposure.histogram(img[:, :, 0]) #histograma de R

        a=histo[1].min() #valor minimo del histograma
        a1=histo[1].max() #valo maximo del histograma

        histo=exposure.histogram(img[:, :, 1]) #histograma de G

        b=histo[1].min() #valor minimo del histograma
        b1=histo[1].max() #valor maximo del histograma

        histo=exposure.histogram(img[:, :, 2]) #histograma de B

        c=histo[1].min() #valor minimo del histograma
        c1=histo[1].max() #valor maximo del histograma

        # parametros para las metricas de la linealizacion de histograma de acuerdo al tipo de imagen

        img = exposure.adjust_log(img,1)

        img[:, :, 0]= exposure.rescale_intensity(img[:, :, 0], in_range=(a, a1)) # 10X=1 / 20X=0.4 / 40x=0.7
        img[:, :, 1]= exposure.rescale_intensity(img[:, :, 1], in_range=(b, b1))   # 10X=2 / 20X=1   / 40x=1.2
        img[:, :, 2]= exposure.rescale_intensity(img[:, :, 2], in_range=(c, c1))   # 10X=4.5 / 20X=12   / 40x=4

        #img = exposure.equalize_adapthist(img)

        io.imshow(img) # imprimir imagen
        io.show()

        io.imsave(raiz+'/linealizacion/'+name[i]+'.png',(img)) #guardar la imagen con linealizado

# Expansión de contraste
def normalizar():
    path = raiz+'/linealizacion' #directorio local de trabajo

    name = listar(path) #llama funcion para listar los archivos .png en el directorio
    nuevaruta = r'mac/40X/normalizacion' #Ruta directorio
    if not os.path.exists(nuevaruta): os.makedirs(nuevaruta) #Validar y crear directorio

    for i in range(len(name)):
        img = io.imread(path+"/"+name[i]+'.png')

        dat = np.array(img)+.0
        sol = np.empty_like(img) #matriz de ceros

        img = exposure.equalize_adapthist(img)

        #formula de Expansión de contraste (255*(imagen-min))/(max-min)

        sol = np.divide(np.multiply(256,np.subtract(img,img.min(),dtype=float),dtype=float),np.subtract(img.max(),img.min(),dtype=float),dtype=float)

        sol = np.uint8(sol) #de Numpy a uint8

        #sol = exposure.adjust_log(sol,1)

        io.imshow(sol) #imprimir imagen
        io.show()

        io.imsave(raiz+'/normalizacion/'+name[i]+'.png',(sol)) #guardar la imagen normalizada

#Hematoxilina - Eosina
def hematoxilina():
    #hacer la umbralizacion manual
    path = raiz+'/normalizacion' #directorio de trabajo local
    name = listar(path) #listar los archivos .png en el directorio
    nuevaruta = r'mac/40X/binario' #Ruta directorio
    if not os.path.exists(nuevaruta): os.makedirs(nuevaruta) #Validar y crear directorio

    for i in range(len(name)):
        ihc_rgb = io.imread(path+"/"+name[i]+'.png')
        ihc_hed = rgb2hed(ihc_rgb) #de RGB a HED (hematoxilina, eosina, dao)
        ihc_hed_np = np.array(ihc_hed) # de Numpy a uint8

        th = threshold_yen(ihc_hed_np[:,:,0])
        #th2 = threshold_otsu(ihc_hed_np[:,:,0])

        sol = ihc_hed_np[:,:,0] <= th
        #sol2 = ihc_hed_np[:,:,0] <= th2

        io.imshow(sol)
        io.show()
        #io.imshow(sol2)
        #io.show()

        sol = np.array(sol*255)

        io.imsave(raiz+'/binario/'+name[i]+'.png',(np.uint8(sol))) #guardar la imagen con binarizado    

def vectores():
    #Declaracion de vectores para graficas

    #Nombres

    n=[]

    #Hibridos Totales
    prof=[]
    anoo=[]
    mess=[]
    area=[]
    peri=[]
    ecce=[]
    circ=[]
    eule=[]
    diam=[]
    majo=[]
    mino=[]

    #Puros Yaque Totales
    prof1=[]
    anoo1=[]
    mess1=[]
    area1=[]
    peri1=[]
    ecce1=[]
    circ1=[]
    eule1=[]
    diam1=[]
    majo1=[]
    mino1=[]

    #Puros Rayado Totales
    prof2=[]
    anoo2=[]
    mess2=[]
    area2=[]
    peri2=[]
    ecce2=[]
    circ2=[]
    eule2=[]
    diam2=[]
    majo2=[]
    mino2=[]

    #Matriz Hibridos
    mprof=[]
    manoo=[]
    mmess=[]
    marea=[]
    mperi=[]
    mecce=[]
    mcirc=[]
    meule=[]
    mdiam=[]
    mmajo=[]
    mmino=[]

    #Matriz Puros Yaque
    mprof1=[]
    manoo1=[]
    mmess1=[]
    marea1=[]
    mperi1=[]
    mecce1=[]
    mcirc1=[]
    meule1=[]
    mdiam1=[]
    mmajo1=[]
    mmino1=[]

    #Matriz Puros Rayado
    mprof2=[]
    manoo2=[]
    mmess2=[]
    marea2=[]
    mperi2=[]
    mecce2=[]
    mcirc2=[]
    meule2=[]
    mdiam2=[]
    mmajo2=[]
    mmino2=[]

    #Auxiliares

    profaux=[]
    anooaux=[]
    messaux=[]
    areaaux=[]
    periaux=[]
    ecceaux=[]
    circaux=[]
    euleaux=[]
    diamaux=[]
    majoaux=[]
    minoaux=[]
    
    return n,prof,anoo,mess,area,peri,ecce,circ,eule,diam,majo,mino,prof1,anoo1,mess1,area1,peri1,ecce1,circ1,eule1,diam1,majo1,mino1,mprof,manoo,mmess,marea,mperi,mecce,mcirc,meule,mdiam,mmajo,mmino,mprof1,manoo1,mmess1,marea1,mperi1,mecce1,mcirc1,meule1,mdiam1,mmajo1,mmino1,profaux,anooaux,messaux,areaaux,periaux,ecceaux,circaux,euleaux,diamaux,majoaux,minoaux,mprof2,manoo2,mmess2,marea2,mperi2,mecce2,mcirc2,meule2,mdiam2,mmajo2,mmino2,prof2,anoo2,mess2,area2,peri2,ecce2,circ2,eule2,diam2,majo2,mino2

def segmentacion(n,prof,anoo,mess,area,peri,ecce,circ,eule,diam,majo,mino,prof1,anoo1,mess1,area1,peri1,ecce1,circ1,eule1,diam1,majo1,mino1,mprof,manoo,mmess,marea,mperi,mecce,mcirc,meule,mdiam,mmajo,mmino,mprof1,manoo1,mmess1,marea1,mperi1,mecce1,mcirc1,meule1,mdiam1,mmajo1,mmino1,profaux,anooaux,messaux,areaaux,periaux,ecceaux,circaux,euleaux,diamaux,majoaux,minoaux,mprof2,manoo2,mmess2,marea2,mperi2,mecce2,mcirc2,meule2,mdiam2,mmajo2,mmino2,prof2,anoo2,mess2,area2,peri2,ecce2,circ2,eule2,diam2,majo2,mino2):

    #whatershep
    path = raiz+'/binario' #directorio de imagenes binarias
    name = listar(path) #listar los archivos .png en el directorio

    try:
        import cPickle as pickle  
    except ImportError:  
        import pickle 

    f = open(raiz+'/40X Micras.txt','w')
    s = open(raiz+'/40X Pixecel.txt','w')

    f.write('nombre,identificador,tipo,profundidad,mes,año,area,perimetro,eccentricity,circularity,euler_number,equivalent_diameter,major_axis_length,minor_axis_length,min_row,min_col,max_row,max_col'+'\n')
    s.write('nombre,identificador,tipo,profundidad,mes,año,area,perimetro,eccentricity,circularity,euler_number,equivalent_diameter,major_axis_length,minor_axis_length,min_row,min_col,max_row,max_col'+'\n')

    for i in range(len(name)):

        image = io.imread(path+"/"+name[i]+'.png')  #leer Imagen
        image = util.invert(image)
        image = np.logical_or(image,0)

        distance = ndimage.distance_transform_edt(image)
        local_maxi = peak_local_max(distance, indices=False, footprint=np.ones((1, 1)), labels=image)

        markers = morphology.label(local_maxi)
        local_ws = watershed(-distance, markers, mask=image)

        #labeling

        label = util.invert(np.divide(local_ws,255))

        all_labels = measure.label(label)
        blobs_labels = measure.label(label, background=255)
        label_img = measure.label(label, connectivity=label.ndim)

        io.imshow(blobs_labels) # imprimir imagen
        io.show()

        #Crear y leer archivos

        prop=regionprops(blobs_labels) #lista de objetos cada objeto con n atributos de forma
        for j in range(len(prop)):
            if(name[i][0]=='a'):
                das='Cefalico'
            elif(name[i][0]=='b'):
                das='Medio'
            elif(name[i][0]=='c'):
                das='Caudal'

            if(name[i][2:4]=='16'):
                if(name[i][6]=='1'):
                    sas='Puros Yaque'
                    mes='mayo'
                    ano='2017'
                elif(name[i][6]=='2'):
                    sas='Puros Rayado'
                    mes='mayo'
                    ano='2017'
            elif(name[i][2:4]=='17'):
                sas='Hibridos'
                if(name[i][6]=='1'):
                    mes='marzo'
                    ano='2017'
                elif(name[i][6]=='2'):
                    mes='mayo'
                    ano='2017'

            if(name[i][0]=='h'):
                sas='Hibridos'

                if(name[i][7:9]=='24'):
                    mes='enero'
                    ano='2017'    
                elif(name[i][7:9]=='26'):
                    mes='noviembre'
                    ano='2016'
                elif(name[i][7:9]=='29'):
                    mes='septiembre'
                    ano='2016'
                elif(name[i][5:9]=='1739'):
                    mes='julio'
                    ano='2016'

                if(name[i][19]=='1' or name[i][19]=='4' or name[i][19]=='7'):
                    das='Cefalico'
                elif(name[i][19]=='2' or name[i][19]=='5' or name[i][19]=='8'):
                    das='Medio'
                elif(name[i][19]=='3' or name[i][19]=='6' or name[i][19]=='9'):
                    das='Caudal'
            if conver2(prop[j].equivalent_diameter,raiz) >= 1.46 and conver2(prop[j].equivalent_diameter,raiz) < 5.9:
                f.write(str(name[i]+'.png')+','+str(j)+','+sas+','+das+','+mes+','+ano+','+str(conver1(prop[j].area,raiz))+','+str(conver2(prop[j].perimeter,raiz))+','+str(prop[j].eccentricity)+','+str((prop[j].area*4*math.pi)/(prop[j].perimeter*prop[j].perimeter))+','+str(prop[j].euler_number)+','+str(conver2(prop[j].equivalent_diameter,raiz))+','+str(conver2(prop[j].major_axis_length,raiz))+','+str(conver2(prop[j].minor_axis_length,raiz))+','+str(prop[j].bbox)+'\n')
                s.write(str(name[i]+'.png')+','+str(j)+','+sas+','+das+','+mes+','+ano+','+str(prop[j].area)+','+str(prop[j].perimeter)+','+str(prop[j].eccentricity)+','+str((prop[j].area*4*math.pi)/(prop[j].perimeter*prop[j].perimeter))+','+str(prop[j].euler_number)+','+str(prop[j].equivalent_diameter)+','+str(prop[j].major_axis_length)+','+str(prop[j].minor_axis_length)+','+str(prop[j].bbox)+'\n')
                n.append(str(name[i]+'.png'))

                if(sas=='Puros Yaque'):
                    prof1.append(das)
                    anoo1.append(ano)
                    mess1.append(mes)
                    area1.append(conver1(prop[j].area,raiz))
                    peri1.append(conver2(prop[j].perimeter,raiz))
                    ecce1.append(prop[j].eccentricity)
                    circ1.append((prop[j].area*4*math.pi)/(prop[j].perimeter*prop[j].perimeter))
                    eule1.append(prop[j].euler_number)
                    diam1.append(conver2(prop[j].equivalent_diameter,raiz))
                    majo1.append(conver2(prop[j].major_axis_length,raiz))
                    mino1.append(conver2(prop[j].minor_axis_length,raiz))
                elif(sas=='Puros Rayado'):
                    prof2.append(das)
                    anoo2.append(ano)
                    mess2.append(mes)
                    area2.append(conver1(prop[j].area,raiz))
                    peri2.append(conver2(prop[j].perimeter,raiz))
                    ecce2.append(prop[j].eccentricity)
                    circ2.append((prop[j].area*4*math.pi)/(prop[j].perimeter*prop[j].perimeter))
                    eule2.append(prop[j].euler_number)
                    diam2.append(conver2(prop[j].equivalent_diameter,raiz))
                    majo2.append(conver2(prop[j].major_axis_length,raiz))
                    mino2.append(conver2(prop[j].minor_axis_length,raiz))
                elif(sas=='Hibridos'):
                    prof.append(das)
                    anoo.append(ano)
                    mess.append(mes)
                    area.append(conver1(prop[j].area,raiz))
                    peri.append(conver2(prop[j].perimeter,raiz))
                    ecce.append(prop[j].eccentricity)
                    circ.append((prop[j].area*4*math.pi)/(prop[j].perimeter*prop[j].perimeter))
                    eule.append(prop[j].euler_number)
                    diam.append(conver2(prop[j].equivalent_diameter,raiz))
                    majo.append(conver2(prop[j].major_axis_length,raiz))
                    mino.append(conver2(prop[j].minor_axis_length,raiz))  
    f.close()
    s.close()

    return n,prof,anoo,mess,area,peri,ecce,circ,eule,diam,majo,mino,prof1,anoo1,mess1,area1,peri1,ecce1,circ1,eule1,diam1,majo1,mino1,mprof,manoo,mmess,marea,mperi,mecce,mcirc,meule,mdiam,mmajo,mmino,mprof1,manoo1,mmess1,marea1,mperi1,mecce1,mcirc1,meule1,mdiam1,mmajo1,mmino1,profaux,anooaux,messaux,areaaux,periaux,ecceaux,circaux,euleaux,diamaux,majoaux,minoaux,mprof2,manoo2,mmess2,marea2,mperi2,mecce2,mcirc2,meule2,mdiam2,mmajo2,mmino2,prof2,anoo2,mess2,area2,peri2,ecce2,circ2,eule2,diam2,majo2,mino2
    
def extraccion(n,prof,anoo,mess,area,peri,ecce,circ,eule,diam,majo,mino,prof1,anoo1,mess1,area1,peri1,ecce1,circ1,eule1,diam1,majo1,mino1,mprof,manoo,mmess,marea,mperi,mecce,mcirc,meule,mdiam,mmajo,mmino,mprof1,manoo1,mmess1,marea1,mperi1,mecce1,mcirc1,meule1,mdiam1,mmajo1,mmino1,profaux,anooaux,messaux,areaaux,periaux,ecceaux,circaux,euleaux,diamaux,majoaux,minoaux,mprof2,manoo2,mmess2,marea2,mperi2,mecce2,mcirc2,meule2,mdiam2,mmajo2,mmino2,prof2,anoo2,mess2,area2,peri2,ecce2,circ2,eule2,diam2,majo2,mino2):
    #Separacion de datos en la matriz
    
    #For para los Puros yaque
  
    lprof = ['Cefalico','Medio','Caudal']
    lano = ['2016','2016','2016','2017','2017','2017']
    lmes = ['julio','septiembre','noviembre','enero','marzo','mayo']
 
    for b in range(len(lprof)):
        for c in range(len(lano)):
            for j in range(len(prof1)):
                if(prof1[j]==lprof[b] and anoo1[j]==lano[c] and mess1[j]==lmes[c]):
                    profaux.append(prof1[j])
                    anooaux.append(anoo1[j])
                    messaux.append(mess1[j])
                    areaaux.append(area1[j])
                    periaux.append(peri1[j])
                    ecceaux.append(ecce1[j])
                    circaux.append(circ1[j])
                    euleaux.append(eule1[j])
                    diamaux.append(diam1[j])
                    majoaux.append(majo1[j])
                    minoaux.append(mino1[j])
            mprof1.append(profaux)
            manoo1.append(anooaux)
            mmess1.append(messaux)
            marea1.append(areaaux)
            mperi1.append(periaux)
            mecce1.append(ecceaux)
            mcirc1.append(circaux)
            meule1.append(euleaux)
            mdiam1.append(diamaux)
            mmajo1.append(majoaux)
            mmino1.append(minoaux)
 
            #Matrices en 0

            profaux=[]
            anooaux=[]
            messaux=[]
            areaaux=[]
            periaux=[]
            ecceaux=[]
            circaux=[]
            euleaux=[]
            diamaux=[]
            majoaux=[]
            minoaux=[]
    
    #For para los Puros Rayado

    for b in range(len(lprof)):
        for c in range(len(lano)):
            for j in range(len(prof2)):
                if(prof2[j]==lprof[b] and anoo2[j]==lano[c] and mess2[j]==lmes[c]):
                    profaux.append(prof2[j])
                    anooaux.append(anoo2[j])
                    messaux.append(mess2[j])
                    areaaux.append(area2[j])
                    periaux.append(peri2[j])
                    ecceaux.append(ecce2[j])
                    circaux.append(circ2[j])
                    euleaux.append(eule2[j])
                    diamaux.append(diam2[j])
                    majoaux.append(majo2[j])
                    minoaux.append(mino2[j])
            mprof2.append(profaux)
            manoo2.append(anooaux)
            mmess2.append(messaux)
            marea2.append(areaaux)
            mperi2.append(periaux)
            mecce2.append(ecceaux)
            mcirc2.append(circaux)
            meule2.append(euleaux)
            mdiam2.append(diamaux)
            mmajo2.append(majoaux)
            mmino2.append(minoaux)
 
            #Matrices en 0

            profaux=[]
            anooaux=[]
            messaux=[]
            areaaux=[]
            periaux=[]
            ecceaux=[]
            circaux=[]
            euleaux=[]
            diamaux=[]
            majoaux=[]
            minoaux=[]

    #For para los Hibridos

    for b in range(len(lprof)):
        for c in range(len(lano)):
            for j in range(len(prof)):
                if(prof[j]==lprof[b] and anoo[j]==lano[c] and mess[j]==lmes[c]):
                    profaux.append(prof[j])
                    anooaux.append(anoo[j])
                    messaux.append(mess[j])
                    areaaux.append(area[j])
                    periaux.append(peri[j])
                    ecceaux.append(ecce[j])
                    circaux.append(circ[j])
                    euleaux.append(eule[j])
                    diamaux.append(diam[j])
                    majoaux.append(majo[j])
                    minoaux.append(mino[j])
            mprof.append(profaux)
            manoo.append(anooaux)
            mmess.append(messaux)
            marea.append(areaaux)
            mperi.append(periaux)
            mecce.append(ecceaux)
            mcirc.append(circaux)
            meule.append(euleaux)
            mdiam.append(diamaux)
            mmajo.append(majoaux)
            mmino.append(minoaux)

            #Matrices en 0

            profaux=[]
            anooaux=[]
            messaux=[]
            areaaux=[]
            periaux=[]
            ecceaux=[]
            circaux=[]
            euleaux=[]
            diamaux=[]
            majoaux=[]
            minoaux=[]

    return n,prof,anoo,mess,area,peri,ecce,circ,eule,diam,majo,mino,prof1,anoo1,mess1,area1,peri1,ecce1,circ1,eule1,diam1,majo1,mino1,mprof,manoo,mmess,marea,mperi,mecce,mcirc,meule,mdiam,mmajo,mmino,mprof1,manoo1,mmess1,marea1,mperi1,mecce1,mcirc1,meule1,mdiam1,mmajo1,mmino1,profaux,anooaux,messaux,areaaux,periaux,ecceaux,circaux,euleaux,diamaux,majoaux,minoaux,mprof2,manoo2,mmess2,marea2,mperi2,mecce2,mcirc2,meule2,mdiam2,mmajo2,mmino2,prof2,anoo2,mess2,area2,peri2,ecce2,circ2,eule2,diam2,majo2,mino2
    
def estadisticos(n,prof,anoo,mess,area,peri,ecce,circ,eule,diam,majo,mino,prof1,anoo1,mess1,area1,peri1,ecce1,circ1,eule1,diam1,majo1,mino1,mprof,manoo,mmess,marea,mperi,mecce,mcirc,meule,mdiam,mmajo,mmino,mprof1,manoo1,mmess1,marea1,mperi1,mecce1,mcirc1,meule1,mdiam1,mmajo1,mmino1,profaux,anooaux,messaux,areaaux,periaux,ecceaux,circaux,euleaux,diamaux,majoaux,minoaux,mprof2,manoo2,mmess2,marea2,mperi2,mecce2,mcirc2,meule2,mdiam2,mmajo2,mmino2,prof2,anoo2,mess2,area2,peri2,ecce2,circ2,eule2,diam2,majo2,mino2):
    #estadisticos

    yaque=[marea1,mperi1,mecce1,mcirc1,meule1,mdiam1,mmajo1,mmino1]
    Rayado=[marea2,mperi2,mecce2,mcirc2,meule2,mdiam2,mmajo2,mmino2]
    hibri=[marea,mperi,mecce,mcirc,meule,mdiam,mmajo,mmino]
    nomb=['Area','Perimetro','Excentricidad','Circularidad','Numero de Euler','Diametro','Longitud Mayor','Longitud Menor']
    prof=['Cefalico','Medio','Caudal']
    
    f = open(raiz+'/40X Estadisticos_micras.txt','w')
    f.write('Medida,profundidad,tipo,mes,año,Mediana,Media,Moda,Varianza Poblacional,Error Estandar,Desviacion Estandar,Maximo,Minimo'+'\n')
    
    #Análisis estadistico Puros Yaque
    i=0
    for a in range(len(prof)):
        for b in range(len(yaque)):
            if yaque[b][0+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',yaque,Julio,2016,'+str(statis.mean(yaque[b][0+i]))+','+str(statis.median(yaque[b][0+i]))+','+str([x for x in set(yaque[b][0+i]) if yaque[b][0+i].count(x) > 1][0] if [x for x in set(yaque[b][0+i]) if yaque[b][0+i].count(x) > 1] else 0)+','+str(statis.pvariance(yaque[b][0+i]))+','+str(stats.sem(yaque[b][0+i]))+','+str(statis.pstdev(yaque[b][0+i]))+','+str(max(yaque[b][0+i]))+','+str(min(yaque[b][0+i]))+'\n')
                print('Yaque juli 2016 '+prof[a])
                print("Mediana "+nomb[b]+' '+str(statis.mean(yaque[b][0+i])))
                print("Media "+nomb[b]+' '+str(statis.median(yaque[b][0+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(yaque[b][0+i]) if yaque[b][0+i].count(x) > 1][0] if [x for x in set(yaque[b][0+i]) if yaque[b][0+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(yaque[b][0+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(yaque[b][0+i])))
                print("Desviacion Estandar"+nomb[b]+' '+str(statis.pstdev(yaque[b][0+i])))
                print()

            if yaque[b][1+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Yaque,Septiembre,2016,'+str(statis.mean(yaque[b][1+i]))+','+str(statis.median(yaque[b][1+i]))+','+str([x for x in set(yaque[b][1+i]) if yaque[b][1+i].count(x) > 1][0] if [x for x in set(yaque[b][1+i]) if yaque[b][1+i].count(x) > 1] else 0)+','+str(statis.pvariance(yaque[b][1+i]))+','+str(stats.sem(yaque[b][1+i]))+','+str(statis.pstdev(yaque[b][1+i]))+','+str(max(yaque[b][1+i]))+','+str(min(yaque[b][1+i]))+'\n')
                print('Yaque sept 2016 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(yaque[b][1+i])))
                print("Media "+nomb[b]+' '+str(statis.median(yaque[b][1+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(yaque[b][1+i]) if yaque[b][1+i].count(x) > 1][0] if [x for x in set(yaque[b][1+i]) if yaque[b][1+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(yaque[b][1+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(yaque[b][1+i])))
                print("Desviacion Estandar"+nomb[b]+' '+str(statis.pstdev(yaque[b][1+i])))
                print()

            if yaque[b][2+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Yaque,Noviembre,2016,'+str(statis.mean(yaque[b][2+i]))+','+str(statis.median(yaque[b][2+i]))+','+str([x for x in set(yaque[b][2+i]) if yaque[b][2+i].count(x) > 1][0] if [x for x in set(yaque[b][2+i]) if yaque[b][2+i].count(x) > 1] else 0)+','+str(statis.pvariance(yaque[b][2+i]))+','+str(stats.sem(yaque[b][2+i]))+','+str(statis.pstdev(yaque[b][2+i]))+','+str(max(yaque[b][2+i]))+','+str(min(yaque[b][2+i]))+'\n')
                print('Yaque novi 2016 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(yaque[b][2+i])))
                print("Media "+nomb[b]+' '+str(statis.median(yaque[b][2+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(yaque[b][2+i]) if yaque[b][2+i].count(x) > 1][0] if [x for x in set(yaque[b][2+i]) if yaque[b][2+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(yaque[b][2+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(yaque[b][2+i])))
                print("Desviacion Estandar"+nomb[b]+' '+str(statis.pstdev(yaque[b][2+i])))
                print()

            if yaque[b][3+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Yaque,Enero,2017,'+str(statis.mean(yaque[b][3+i]))+','+str(statis.median(yaque[b][3+i]))+','+str([x for x in set(yaque[b][3+i]) if yaque[b][3+i].count(x) > 1][0] if [x for x in set(yaque[b][3+i]) if yaque[b][3+i].count(x) > 1] else 0)+','+str(statis.pvariance(yaque[b][3+i]))+','+str(stats.sem(yaque[b][3+i]))+','+str(statis.pstdev(yaque[b][3+i]))+','+str(max(yaque[b][3+i]))+','+str(min(yaque[b][3+i]))+'\n')
                print('Yaque ener 2017 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(yaque[b][3+i])))
                print("Media "+nomb[b]+' '+str(statis.median(yaque[b][3+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(yaque[b][3+i]) if yaque[b][3+i].count(x) > 1][0] if [x for x in set(yaque[b][3+i]) if yaque[b][3+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(yaque[b][3+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(yaque[b][3+i])))
                print("Desviacion Estandar"+nomb[b]+' '+str(statis.pstdev(yaque[b][3+i])))
                print()

            if yaque[b][4+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Yaque,marzo,2017,'+str(statis.mean(yaque[b][4+i]))+','+str(statis.median(yaque[b][4+i]))+','+str([x for x in set(yaque[b][4+i]) if yaque[b][4+i].count(x) > 1][0] if [x for x in set(yaque[b][4+i]) if yaque[b][4+i].count(x) > 1] else 0)+','+str(statis.pvariance(yaque[b][4+i]))+','+str(stats.sem(yaque[b][4+i]))+','+str(statis.pstdev(yaque[b][4+i]))+','+str(max(yaque[b][4+i]))+','+str(min(yaque[b][4+i]))+'\n')
                print('Yaque marz 2017 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(yaque[b][4+i])))
                print("Media "+nomb[b]+' '+str(statis.median(yaque[b][4+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(yaque[b][4+i]) if yaque[b][4+i].count(x) > 1][0] if [x for x in set(yaque[b][4+i]) if yaque[b][4+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(yaque[b][4+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(yaque[b][4+i])))
                print("Desviacion Estandar"+nomb[b]+' '+str(statis.pstdev(yaque[b][4+i])))
                print()

            if yaque[b][5+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Yaque,Mayo,2017,'+str(statis.mean(yaque[b][5+i]))+','+str(statis.median(yaque[b][5+i]))+','+str([x for x in set(yaque[b][5+i]) if yaque[b][5+i].count(x) > 1][0] if [x for x in set(yaque[b][5+i]) if yaque[b][5+i].count(x) > 1] else 0)+','+str(statis.pvariance(yaque[b][5+i]))+','+str(stats.sem(yaque[b][5+i]))+','+str(statis.pstdev(yaque[b][5+i]))+','+str(max(yaque[b][5+i]))+','+str(min(yaque[b][5+i]))+'\n')
                print('Yaque mayo 2017 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(yaque[b][5+i])))
                print("Media "+nomb[b]+' '+str(statis.median(yaque[b][5+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(yaque[b][5+i]) if yaque[b][5+i].count(x) > 1][0] if [x for x in set(yaque[b][5+i]) if yaque[b][5+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(yaque[b][5+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(yaque[b][5+i])))
                print("Desviacion Estandar"+nomb[b]+' '+str(statis.pstdev(yaque[b][5+i])))
                print()

        i=i+6

    #Análisis estadistico Puros Rayado
    i=0
    for a in range(len(prof)):
        for b in range(len(Rayado)):
            if Rayado[b][0+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Rayado,Julio,2016,'+str(statis.mean(Rayado[b][0+i]))+','+str(statis.median(Rayado[b][0+i]))+','+str([x for x in set(Rayado[b][0+i]) if Rayado[b][0+i].count(x) > 1][0] if [x for x in set(Rayado[b][0+i]) if Rayado[b][0+i].count(x) > 1] else 0)+','+str(statis.pvariance(Rayado[b][0+i]))+','+str(stats.sem(Rayado[b][0+i]))+','+str(statis.pstdev(Rayado[b][0+i]))+','+str(max(Rayado[b][0+i]))+','+str(min(Rayado[b][0+i]))+'\n')
                print('Rayado juli 2016 '+prof[a])
                print("Mediana "+nomb[b]+' '+str(statis.mean(Rayado[b][0+i])))
                print("Media "+nomb[b]+' '+str(statis.median(Rayado[b][0+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(Rayado[b][0+i]) if Rayado[b][0+i].count(x) > 1][0] if [x for x in set(Rayado[b][0+i]) if Rayado[b][0+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(Rayado[b][0+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(Rayado[b][0+i])))
                print("Desviacion Estandar"+nomb[b]+' '+str(statis.pstdev(Rayado[b][0+i])))
                print()

            if Rayado[b][1+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Rayado,Septiembre,2016,'+str(statis.mean(Rayado[b][1+i]))+','+str(statis.median(Rayado[b][1+i]))+','+str([x for x in set(Rayado[b][1+i]) if Rayado[b][1+i].count(x) > 1][0] if [x for x in set(Rayado[b][1+i]) if Rayado[b][1+i].count(x) > 1] else 0)+','+str(statis.pvariance(Rayado[b][1+i]))+','+str(stats.sem(Rayado[b][1+i]))+','+str(statis.pstdev(Rayado[b][1+i]))+','+str(max(Rayado[b][1+i]))+','+str(min(Rayado[b][1+i]))+'\n')
                print('Rayado sept 2016 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(Rayado[b][1+i])))
                print("Media "+nomb[b]+' '+str(statis.median(Rayado[b][1+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(Rayado[b][1+i]) if Rayado[b][1+i].count(x) > 1][0] if [x for x in set(Rayado[b][1+i]) if Rayado[b][1+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(Rayado[b][1+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(Rayado[b][1+i])))
                print("Desviacion Estandar"+nomb[b]+' '+str(statis.pstdev(Rayado[b][1+i])))
                print()

            if Rayado[b][2+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Rayado,Noviembre,2016,'+str(statis.mean(Rayado[b][2+i]))+','+str(statis.median(Rayado[b][2+i]))+','+str([x for x in set(Rayado[b][2+i]) if Rayado[b][2+i].count(x) > 1][0] if [x for x in set(Rayado[b][2+i]) if Rayado[b][2+i].count(x) > 1] else 0)+','+str(statis.pvariance(Rayado[b][2+i]))+','+str(stats.sem(Rayado[b][2+i]))+','+str(statis.pstdev(Rayado[b][2+i]))+','+str(max(Rayado[b][2+i]))+','+str(min(Rayado[b][2+i]))+'\n')
                print('Rayado novi 2016 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(Rayado[b][2+i])))
                print("Media "+nomb[b]+' '+str(statis.median(Rayado[b][2+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(Rayado[b][2+i]) if Rayado[b][2+i].count(x) > 1][0] if [x for x in set(Rayado[b][2+i]) if Rayado[b][2+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(Rayado[b][2+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(Rayado[b][2+i])))
                print("Desviacion Estandar"+nomb[b]+' '+str(statis.pstdev(Rayado[b][2+i])))
                print()

            if Rayado[b][3+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Rayado,Enero,2017,'+str(statis.mean(Rayado[b][3+i]))+','+str(statis.median(Rayado[b][3+i]))+','+str([x for x in set(Rayado[b][3+i]) if Rayado[b][3+i].count(x) > 1][0] if [x for x in set(Rayado[b][3+i]) if Rayado[b][3+i].count(x) > 1] else 0)+','+str(statis.pvariance(Rayado[b][3+i]))+','+str(stats.sem(Rayado[b][3+i]))+','+str(statis.pstdev(Rayado[b][3+i]))+','+str(max(Rayado[b][3+i]))+','+str(min(Rayado[b][3+i]))+'\n')
                print('Rayado ener 2017 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(Rayado[b][3+i])))
                print("Media "+nomb[b]+' '+str(statis.median(Rayado[b][3+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(Rayado[b][3+i]) if Rayado[b][3+i].count(x) > 1][0] if [x for x in set(Rayado[b][3+i]) if Rayado[b][3+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(Rayado[b][3+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(Rayado[b][3+i])))
                print("Desviacion Estandar"+nomb[b]+' '+str(statis.pstdev(Rayado[b][3+i])))
                print()

            if Rayado[b][4+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Rayado,marzo,2017,'+str(statis.mean(Rayado[b][4+i]))+','+str(statis.median(Rayado[b][4+i]))+','+str([x for x in set(Rayado[b][4+i]) if Rayado[b][4+i].count(x) > 1][0] if [x for x in set(Rayado[b][4+i]) if Rayado[b][4+i].count(x) > 1] else 0)+','+str(statis.pvariance(Rayado[b][4+i]))+','+str(stats.sem(Rayado[b][4+i]))+','+str(statis.pstdev(Rayado[b][4+i]))+','+str(max(Rayado[b][4+i]))+','+str(min(Rayado[b][4+i]))+'\n')
                print('Rayado marz 2017 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(Rayado[b][4+i])))
                print("Media "+nomb[b]+' '+str(statis.median(Rayado[b][4+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(Rayado[b][4+i]) if Rayado[b][4+i].count(x) > 1][0] if [x for x in set(Rayado[b][4+i]) if Rayado[b][4+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(Rayado[b][4+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(Rayado[b][4+i])))
                print("Desviacion Estandar"+nomb[b]+' '+str(statis.pstdev(Rayado[b][4+i])))
                print()

            if Rayado[b][5+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Rayado,Mayo,2017,'+str(statis.mean(Rayado[b][5+i]))+','+str(statis.median(Rayado[b][5+i]))+','+str([x for x in set(Rayado[b][5+i]) if Rayado[b][5+i].count(x) > 1][0] if [x for x in set(Rayado[b][5+i]) if Rayado[b][5+i].count(x) > 1] else 0)+','+str(statis.pvariance(Rayado[b][5+i]))+','+str(stats.sem(Rayado[b][5+i]))+','+str(statis.pstdev(Rayado[b][5+i]))+','+str(max(Rayado[b][5+i]))+','+str(min(Rayado[b][5+i]))+'\n')
                print('Rayado mayo 2017 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(Rayado[b][5+i])))
                print("Media "+nomb[b]+' '+str(statis.median(Rayado[b][5+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(Rayado[b][5+i]) if Rayado[b][5+i].count(x) > 1][0] if [x for x in set(Rayado[b][5+i]) if Rayado[b][5+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(Rayado[b][5+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(Rayado[b][5+i])))
                print("Desviacion Estandar"+nomb[b]+' '+str(statis.pstdev(Rayado[b][5+i])))
                print()

        i=i+6

    #Análisis estadistico Hibridos
    i=0
    for a in range(len(prof)):
        for b in range(len(hibri)):
            if hibri[b][0+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Hibrido,Julio,2016,'+str(statis.mean(hibri[b][0+i]))+','+str(statis.median(hibri[b][0+i]))+','+str([x for x in set(hibri[b][0+i]) if hibri[b][0+i].count(x) > 1][0] if [x for x in set(hibri[b][0+i]) if hibri[b][0+i].count(x) > 1] else 0)+','+str(statis.pvariance(hibri[b][0+i]))+','+str(stats.sem(hibri[b][0+i]))+','+str(statis.pstdev(hibri[b][0+i]))+','+str(max(hibri[b][0+i]))+','+str(min(hibri[b][0+i]))+'\n')
                print('Hibrido juli 2016 '+prof[a])
                print("Mediana "+nomb[b]+' '+str(statis.mean(hibri[b][0+i])))
                print("Media "+nomb[b]+' '+str(statis.median(hibri[b][0+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(hibri[b][0+i]) if hibri[b][0+i].count(x) > 1][0] if [x for x in set(hibri[b][0+i]) if hibri[b][0+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(hibri[b][0+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(hibri[b][0+i])))
                print("Desviacion Estandar"+nomb[b]+' '+str(statis.pstdev(hibri[b][0+i])))
                print()

            if hibri[b][1+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Hibrido,Septiembre,2016,'+str(statis.mean(hibri[b][1+i]))+','+str(statis.median(hibri[b][1+i]))+','+str([x for x in set(hibri[b][1+i]) if hibri[b][1+i].count(x) > 1][0] if [x for x in set(hibri[b][1+i]) if hibri[b][1+i].count(x) > 1] else 0)+','+str(statis.pvariance(hibri[b][1+i]))+','+str(stats.sem(hibri[b][1+i]))+','+str(statis.pstdev(hibri[b][1+i]))+','+str(max(hibri[b][1+i]))+','+str(min(hibri[b][1+i]))+'\n')
                print('Hibrido sept 2016 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(hibri[b][1+i])))
                print("Media "+nomb[b]+' '+str(statis.median(hibri[b][1+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(hibri[b][1+i]) if hibri[b][1+i].count(x) > 1][0] if [x for x in set(hibri[b][1+i]) if hibri[b][1+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(hibri[b][1+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(hibri[b][1+i])))
                print("Desviacion Estandar"+nomb[b]+' '+str(statis.pstdev(hibri[b][1+i])))
                print()

            if hibri[b][2+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Hibrido,Noviembre,2016,'+str(statis.mean(hibri[b][2+i]))+','+str(statis.median(hibri[b][2+i]))+','+str([x for x in set(hibri[b][2+i]) if hibri[b][2+i].count(x) > 1][0] if [x for x in set(hibri[b][2+i]) if hibri[b][2+i].count(x) > 1] else 0)+','+str(statis.pvariance(hibri[b][2+i]))+','+str(stats.sem(hibri[b][2+i]))+','+str(statis.pstdev(hibri[b][2+i]))+','+str(max(hibri[b][2+i]))+','+str(min(hibri[b][2+i]))+'\n')
                print('Hibrido novi 2016 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(hibri[b][2+i])))
                print("Media "+nomb[b]+' '+str(statis.median(hibri[b][2+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(hibri[b][2+i]) if hibri[b][2+i].count(x) > 1][0] if [x for x in set(hibri[b][2+i]) if hibri[b][2+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(hibri[b][2+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(hibri[b][2+i])))
                print("Desviacion Estandar"+nomb[b]+' '+str(statis.pstdev(hibri[b][2+i])))
                print()

            if hibri[b][3+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Hibrido,Enero,2017,'+str(statis.mean(hibri[b][3+i]))+','+str(statis.median(hibri[b][3+i]))+','+str([x for x in set(hibri[b][3+i]) if hibri[b][3+i].count(x) > 1][0] if [x for x in set(hibri[b][3+i]) if hibri[b][3+i].count(x) > 1] else 0)+','+str(statis.pvariance(hibri[b][3+i]))+','+str(stats.sem(hibri[b][3+i]))+','+str(statis.pstdev(hibri[b][3+i]))+','+str(max(hibri[b][3+i]))+','+str(min(hibri[b][3+i]))+'\n')
                print('Hibrido ener 2017 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(hibri[b][3+i])))
                print("Media "+nomb[b]+' '+str(statis.median(hibri[b][3+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(hibri[b][3+i]) if hibri[b][3+i].count(x) > 1][0] if [x for x in set(hibri[b][3+i]) if hibri[b][3+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(hibri[b][3+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(hibri[b][3+i])))
                print("Desviacion Estandar"+nomb[b]+' '+str(statis.pstdev(hibri[b][3+i])))
                print()

            if hibri[b][4+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Hibrido,Marzo,2017,'+str(statis.mean(hibri[b][4+i]))+','+str(statis.median(hibri[b][4+i]))+','+str([x for x in set(hibri[b][4+i]) if hibri[b][4+i].count(x) > 1][0] if [x for x in set(hibri[b][4+i]) if hibri[b][4+i].count(x) > 1] else 0)+','+str(statis.pvariance(hibri[b][4+i]))+','+str(stats.sem(hibri[b][4+i]))+','+str(statis.pstdev(hibri[b][4+i]))+','+str(max(hibri[b][4+i]))+','+str(min(hibri[b][4+i]))+'\n')
                print('Hibrido marz 2017 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(hibri[b][4+i])))
                print("Media "+nomb[b]+' '+str(statis.median(hibri[b][4+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(hibri[b][4+i]) if hibri[b][4+i].count(x) > 1][0] if [x for x in set(hibri[b][4+i]) if hibri[b][4+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(hibri[b][4+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(hibri[b][4+i])))
                print("Desviacion Estandar"+nomb[b]+' '+str(statis.pstdev(hibri[b][4+i])))
                print()

            if hibri[b][5+i]!=[]:
                f.write(nomb[b]+','+prof[a]+',Hibrido,Mayo,2017,'+str(statis.mean(hibri[b][5+i]))+','+str(statis.median(hibri[b][5+i]))+','+str([x for x in set(hibri[b][5+i]) if hibri[b][5+i].count(x) > 1][0] if [x for x in set(hibri[b][5+i]) if hibri[b][5+i].count(x) > 1] else 0)+','+str(statis.pvariance(hibri[b][5+i]))+','+str(stats.sem(hibri[b][5+i]))+','+str(statis.pstdev(hibri[b][5+i]))+','+str(max(hibri[b][5+i]))+','+str(min(hibri[b][5+i]))+'\n')
                print('Hibrido mayo 2017 '+prof[a])    
                print("Mediana "+nomb[b]+' '+str(statis.mean(hibri[b][5+i])))
                print("Media "+nomb[b]+' '+str(statis.median(hibri[b][5+i])))
                print("Moda "+nomb[b]+' '+str([x for x in set(hibri[b][5+i]) if hibri[b][5+i].count(x) > 1][0] if [x for x in set(hibri[b][5+i]) if hibri[b][5+i].count(x) > 1] else 0))
                print("Varianza Poblacional "+nomb[b]+' '+str(statis.pvariance(hibri[b][5+i])))
                print("Error Estandar Media"+nomb[b]+' '+str(stats.sem(hibri[b][5+i])))
                print("Desviacion Estandar"+nomb[b]+' '+str(statis.pstdev(hibri[b][5+i])))
                print()

        i=i+6
    
    f.close()
    return(yaque,Rayado,hibri,nomb,prof,area,peri,ecce,circ,eule,diam,majo,mino,area1,peri1,ecce1,circ1,eule1,diam1,majo1,mino1,area2,peri2,ecce2,circ2,eule2,diam2,majo2,mino2)
        
def histogramas(yaque,Rayado,hibri,nomb,prof,area,peri,ecce,circ,eule,diam,majo,mino,area1,peri1,ecce1,circ1,eule1,diam1,majo1,mino1,area2,peri2,ecce2,circ2,eule2,diam2,majo2,mino2):
    #histogramas

    bins = np.linspace(0, 5, 12)

    plt.hist([area1,area2,area], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Machos Área")
    plt.savefig(raiz+"mac-area.png",bbox_inches='tight')
    plt.show()

    plt.hist([peri1,peri2,peri], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Machos Perimetro")
    plt.savefig(raiz+"Mac-perimetro.png",bbox_inches='tight')
    plt.show()

    plt.hist([ecce1,ecce2,ecce], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Machos Excentricidad")
    plt.savefig(raiz+"Mac-excentricidad.png",bbox_inches='tight')
    plt.show()

    plt.hist([circ1,circ2,circ], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Machos Circularidad")
    plt.savefig(raiz+"Mac-circularidad.png",bbox_inches='tight')
    plt.show()

    plt.hist([eule1,eule2,eule], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Machos Numero de Euler")
    plt.savefig(raiz+"Mac-numero_euler.png",bbox_inches='tight')
    plt.show()

    plt.hist([diam1,diam2,diam], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Machos Diametro")
    plt.savefig(raiz+"Mac-Diametro.png",bbox_inches='tight')
    plt.show()

    plt.hist([majo1,majo2,majo], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Machos Longitud del eje mayor")
    plt.savefig(raiz+"Mac-Longitud_mayor.png",bbox_inches='tight')
    plt.show()

    plt.hist([mino1,mino2,mino], range=[0, 2], label=['Yaque','Rayado','Hibridos'])
    plt.legend(loc='upper right')
    plt.title("Histograma Machos Longitud del eje menor")
    plt.savefig(raiz+"Mac-Longitud_menor.png",bbox_inches='tight')
    plt.show()
        
def cajas(yaque,Rayado,hibri,nomb):
    
    #Cajas Puros yaque Cortes

    for b in range(len(yaque)):
        
        if b == 0:
            etiq = u'Tamaño (µm ^ 2)'
        elif b in [1,5,6,7]:
            etiq = u'Tamaño (µm)'
        else:
            etiq = ''
        
        if(yaque[b][0]!=[] or yaque[b][6]!=[] or yaque[b][12]!=[]):
            plt.ion()
            plt.boxplot([yaque[b][0],yaque[b][6],yaque[b][12]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Yaque Machos Julio 2016')
            plt.savefig(raiz+nomb[b]+"-Yaque-Julio.png",bbox_inches='tight')
            plt.show()

        if(yaque[b][1]!=[] or yaque[b][7]!=[] or yaque[b][13]!=[]):
            plt.ion()
            plt.boxplot([yaque[b][1],yaque[b][7],yaque[b][13]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Yaque Machos Septiembre 2016')
            plt.savefig(raiz+nomb[b]+"-Yaque-Septiembre.png",bbox_inches='tight')
            plt.show()

        if(yaque[b][2]!=[] or yaque[b][8]!=[] or yaque[b][14]!=[]):
            plt.ion()
            plt.boxplot([yaque[b][2],yaque[b][8],yaque[b][14]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Yaque Machos Noviembre 2016')
            plt.savefig(raiz+nomb[b]+"-Yaque-Noviembre.png",bbox_inches='tight')
            plt.show()

        if(yaque[b][3]!=[] or yaque[b][9]!=[] or yaque[b][15]!=[]):
            plt.ion()
            plt.boxplot([yaque[b][3],yaque[b][9],yaque[b][15]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Yaque Machos Enero 2017')
            plt.savefig(raiz+nomb[b]+"-Yaque-Enero.png",bbox_inches='tight')
            plt.show()

        if(yaque[b][4]!=[] or yaque[b][10]!=[] or yaque[b][16]!=[]):
            plt.ion()
            plt.boxplot([yaque[b][4],yaque[b][10],yaque[b][16]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Yaque Machos Marzo 2017')
            plt.savefig(raiz+nomb[b]+"-Yaque-Marzo.png",bbox_inches='tight')
            plt.show()

        if(yaque[b][5]!=[] or yaque[b][11]!=[] or yaque[b][17]!=[]):
            plt.ion()
            plt.boxplot([yaque[b][5],yaque[b][11],yaque[b][17]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Yaque Machos Mayo 2017')
            plt.savefig(raiz+nomb[b]+"-Yaque-Mayo.png",bbox_inches='tight')
            plt.show()

    #Cajas Puros Rayado Cortes

    for b in range(len(Rayado)):
        
        if b == 0:
            etiq = u'Tamaño (µm ^ 2)'
        elif b in [1,5,6,7]:
            etiq = u'Tamaño (µm)'
        else:
            etiq = ''
        
        if(Rayado[b][0]!=[] or Rayado[b][6]!=[] or Rayado[b][12]!=[]):
            plt.ion()
            plt.boxplot([Rayado[b][0],Rayado[b][6],Rayado[b][12]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Rayado Machos Julio 2016')
            plt.savefig(raiz+nomb[b]+"-Rayado-Julio.png",bbox_inches='tight')
            plt.show()

        if(Rayado[b][1]!=[] or Rayado[b][7]!=[] or Rayado[b][13]!=[]):
            plt.ion()
            plt.boxplot([Rayado[b][1],Rayado[b][7],Rayado[b][13]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Rayado Machos Septiembre 2016')
            plt.savefig(raiz+nomb[b]+"-Rayado-Septiembre.png",bbox_inches='tight')
            plt.show()

        if(Rayado[b][2]!=[] or Rayado[b][8]!=[] or Rayado[b][14]!=[]):
            plt.ion()
            plt.boxplot([Rayado[b][2],Rayado[b][8],Rayado[b][14]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Rayado Machos Noviembre 2016')
            plt.savefig(raiz+nomb[b]+"-Rayado-Noviembre.png",bbox_inches='tight')
            plt.show()

        if(Rayado[b][3]!=[] or Rayado[b][9]!=[] or Rayado[b][15]!=[]):
            plt.ion()
            plt.boxplot([Rayado[b][3],Rayado[b][9],Rayado[b][15]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Rayado Machos Enero 2017')
            plt.savefig(raiz+nomb[b]+"-Rayado-Enero.png",bbox_inches='tight')
            plt.show()

        if(Rayado[b][4]!=[] or Rayado[b][10]!=[] or Rayado[b][16]!=[]):
            plt.ion()
            plt.boxplot([Rayado[b][4],Rayado[b][10],Rayado[b][16]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Rayado Machos Marzo 2017')
            plt.savefig(raiz+nomb[b]+"-Rayado-Marzo.png",bbox_inches='tight')
            plt.show()

        if(Rayado[b][5]!=[] or Rayado[b][11]!=[] or Rayado[b][17]!=[]):
            plt.ion()
            plt.boxplot([Rayado[b][5],Rayado[b][11],Rayado[b][17]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Rayado Machos Mayo 2017')
            plt.savefig(raiz+nomb[b]+"-Rayado-Mayo.png",bbox_inches='tight')
            plt.show()

    #Cajas Hibridos Cortes
    
    for b in range(len(hibri)):
        
        if b == 0:
            etiq = u'Tamaño (µm ^ 2)'
        elif b in [1,5,6,7]:
            etiq = u'Tamaño (µm)'
        else:
            etiq = ''
        
        if(hibri[b][0]!=[] or hibri[b][6]!=[] or hibri[b][12]!=[]):
            plt.ion()
            plt.boxplot([hibri[b][0],hibri[b][6],hibri[b][12]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Hibridos Machos Julio 2016')
            plt.savefig(raiz+nomb[b]+"-Hibrido-Julio.png",bbox_inches='tight')
            plt.show()

        if(hibri[b][1]!=[] or hibri[b][7]!=[] or hibri[b][13]!=[]):
            plt.ion()
            plt.boxplot([hibri[b][1],hibri[b][7],hibri[b][13]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Hibridos Machos Septiembre 2016')
            plt.savefig(raiz+nomb[b]+"-Hibrido-Septiembre.png",bbox_inches='tight')
            plt.show()

        if(hibri[b][2]!=[] or hibri[b][8]!=[] or hibri[b][14]!=[]):
            plt.ion()
            plt.boxplot([hibri[b][2],hibri[b][8],hibri[b][14]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Hibridos Machos Noviembre 2016')
            plt.savefig(raiz+nomb[b]+"-Hibrido-Noviembre.png",bbox_inches='tight')
            plt.show()

        if(hibri[b][3]!=[] or hibri[b][9]!=[] or hibri[b][15]!=[]):
            plt.ion()
            plt.boxplot([hibri[b][3],hibri[b][9],hibri[b][15]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Hibridos Machos Enero 2017')
            plt.savefig(raiz+nomb[b]+"-Hibrido-Enero.png",bbox_inches='tight')
            plt.show()

        if(hibri[b][4]!=[] or hibri[b][10]!=[] or hibri[b][16]!=[]):
            plt.ion()
            plt.boxplot([hibri[b][4],hibri[b][10],hibri[b][16]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Hibridos Machos Marzo 2017')
            plt.savefig(raiz+nomb[b]+"-Hibrido-Marzo.png",bbox_inches='tight')
            plt.show()

        if(hibri[b][5]!=[] or hibri[b][11]!=[] or hibri[b][17]!=[]):
            plt.ion()
            plt.boxplot([hibri[b][5],hibri[b][11],hibri[b][17]])
            plt.xticks([1,2,3], ['Cefalico', 'Medio', 'Caudal'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
            plt.ylabel(etiq)
            plt.title(nomb[b]+' Hibridos Machos Mayo 2017')
            plt.savefig(raiz+nomb[b]+"-Hibrido-Mayo.png",bbox_inches='tight')
            plt.show()

    for b in range(len(hibri)):
        
        if b == 0:
            etiq = u'Tamaño (µm ^ 2)'
        elif b in [1,5,6,7]:
            etiq = u'Tamaño (µm)'
        else:
            etiq = ''

        plt.ion()
        plt.boxplot([hibri[b][0]+hibri[b][6]+hibri[b][12],hibri[b][1]+hibri[b][7]+hibri[b][13],hibri[b][2]+hibri[b][8]+hibri[b][14],hibri[b][3]+hibri[b][9]+hibri[b][15],hibri[b][4]+hibri[b][10]+hibri[b][16],hibri[b][5]+hibri[b][11]+hibri[b][17]])
        plt.xticks([1,2,3,4,5,6], ['jul 2016','sep 2016','nov 2016','ene 2017','mar 2017','may 2017'], size = 'small', color = 'k')  # Colocamos las etiquetas para cada distribución
        plt.ylabel(etiq)
        plt.title(nomb[b] +' hibridos Machos')
        plt.savefig(raiz+nomb[b]+"-Hibridos-Hem.png",bbox_inches='tight')
        plt.show()

def diagrama_medias(yaque,Rayado,hibri,nomb):
    
    #Diagrama medias Puros Rayado Cortes
    x_aux=[]
    y_aux=[]
    yerr_aux=[]
    lista=[]

    lista1=[]
    lista2=[]
    lista3=[]

    x=[]
    y=[]
    yerr=[]

    for b in range(len(yaque)):
        if(yaque[b][0]!=[] or yaque[b][6]!=[] or yaque[b][12]!=[]):
            lista+=yaque[b][0]
            lista+=yaque[b][6]
            lista+=yaque[b][12]
            x_aux.append('Jul 2016')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(yaque[b][1]!=[] or yaque[b][7]!=[] or yaque[b][13]!=[]):
            lista+=yaque[b][1]
            lista+=yaque[b][7]
            lista+=yaque[b][13]
            x_aux.append('Sep 2016')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(yaque[b][2]!=[] or yaque[b][8]!=[] or yaque[b][14]!=[]):
            lista+=yaque[b][2]
            lista+=yaque[b][8]
            lista+=yaque[b][14]
            x_aux.append('Nov 2016')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(yaque[b][3]!=[] or yaque[b][9]!=[] or yaque[b][15]!=[]):
            lista+=yaque[b][3]
            lista+=yaque[b][9]
            lista+=yaque[b][15]
            x_aux.append('Ene 2017')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(yaque[b][4]!=[] or yaque[b][10]!=[] or yaque[b][16]!=[]):
            lista+=yaque[b][4]
            lista+=yaque[b][10]
            lista+=yaque[b][16]
            x_aux.append('Mar 2017')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(yaque[b][5]!=[] or yaque[b][11]!=[] or yaque[b][17]!=[]):
            lista+=yaque[b][5]
            lista+=yaque[b][11]
            lista+=yaque[b][17]
            x_aux.append('May 2017')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        x.append(x_aux)
        y.append(y_aux)
        yerr.append(yerr_aux)
        x_aux=[]
        y_aux=[]
        yerr_aux=[]

    #Diagrama medias Puros Rayado Cortes

    x1=[]
    y1=[]
    yerr1=[]

    for b in range(len(Rayado)):
        if(Rayado[b][0]!=[] or Rayado[b][6]!=[] or Rayado[b][12]!=[]):
            lista+=Rayado[b][0]
            lista+=Rayado[b][6]
            lista+=Rayado[b][12]
            x_aux.append('Jul 2016')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(Rayado[b][1]!=[] or Rayado[b][7]!=[] or Rayado[b][13]!=[]):
            lista+=Rayado[b][1]
            lista+=Rayado[b][7]
            lista+=Rayado[b][13]
            x_aux.append('Sep 2016')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(Rayado[b][2]!=[] or Rayado[b][8]!=[] or Rayado[b][14]!=[]):
            lista+=Rayado[b][2]
            lista+=Rayado[b][8]
            lista+=Rayado[b][14]
            x_aux.append('Nov 2016')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(Rayado[b][3]!=[] or Rayado[b][9]!=[] or Rayado[b][15]!=[]):
            lista+=Rayado[b][3]
            lista+=Rayado[b][9]
            lista+=Rayado[b][15]
            x_aux.append('Ene 2017')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(Rayado[b][4]!=[] or Rayado[b][10]!=[] or Rayado[b][16]!=[]):
            lista+=Rayado[b][4]
            lista+=Rayado[b][10]
            lista+=Rayado[b][16]
            x_aux.append('Mar 2017')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(Rayado[b][5]!=[] or Rayado[b][11]!=[] or Rayado[b][17]!=[]):
            lista+=Rayado[b][5]
            lista+=Rayado[b][11]
            lista+=Rayado[b][17]
            x_aux.append('May 2017')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        x1.append(x_aux)
        y1.append(y_aux)
        yerr1.append(yerr_aux)
        x_aux=[]
        y_aux=[]
        yerr_aux=[]

    #Diagrama medias Hibridos Cortes

    x2=[]
    y2=[]
    yerr2=[]

    for b in range(len(hibri)):
        if(hibri[b][0]!=[] or hibri[b][6]!=[] or hibri[b][12]!=[]):
            lista+=hibri[b][0]
            lista+=hibri[b][6]
            lista+=hibri[b][12]
            x_aux.append('Jul 2016')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(hibri[b][1]!=[] or hibri[b][7]!=[] or hibri[b][13]!=[]):
            lista+=hibri[b][1]
            lista+=hibri[b][7]
            lista+=hibri[b][13]
            x_aux.append('Sep 2016')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(hibri[b][2]!=[] or hibri[b][8]!=[] or hibri[b][14]!=[]):
            lista+=hibri[b][2]
            lista+=hibri[b][8]
            lista+=hibri[b][14]
            x_aux.append('Nov 2016')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(hibri[b][3]!=[] or hibri[b][9]!=[] or hibri[b][15]!=[]):
            lista+=hibri[b][3]
            lista+=hibri[b][9]
            lista+=hibri[b][15]
            x_aux.append('Ene 2017')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(hibri[b][4]!=[] or hibri[b][10]!=[] or hibri[b][16]!=[]):
            lista+=hibri[b][4]
            lista+=hibri[b][10]
            lista+=hibri[b][16]
            x_aux.append('Mar 2017')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        if(hibri[b][5]!=[] or hibri[b][11]!=[] or hibri[b][17]!=[]):
            lista+=hibri[b][5]
            lista+=hibri[b][11]
            lista+=hibri[b][17]
            x_aux.append('May 2017')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista=[]

        x2.append(x_aux)
        y2.append(y_aux)
        yerr2.append(yerr_aux)
        x_aux=[]
        y_aux=[]
        yerr_aux=[]

    for b in range(len(nomb)):

        if b == 0:
            etiq = u'Tamaño (µm ^ 2)'
        elif b in [1,5,6,7]:
            etiq = u'Tamaño (µm)'
        else:
            etiq = ''

        plt.errorbar(x[b], y[b], yerr[b], marker='s', ecolor='green', mfc='black',mec='green')
        plt.title(nomb[b]+' Yaque Machos')
        plt.ylabel(etiq)
        plt.savefig(raiz+nomb[b]+"-yaque-mac.png",bbox_inches='tight')
        plt.show()
        plt.errorbar(x1[b], y1[b], yerr1[b], marker='s', ecolor='blue', mfc='violet',mec='blue')
        plt.title(nomb[b]+' Rayado Machos')
        plt.ylabel(etiq)
        plt.savefig(raiz+nomb[b]+"-Rayado-mac.png",bbox_inches='tight')
        plt.show()
        plt.errorbar(x2[b], y2[b], yerr2[b], marker='s', ecolor='red', mfc='pink',mec='red')
        plt.title(nomb[b]+' Hibridos Machos')
        plt.ylabel(etiq)
        plt.savefig(raiz+nomb[b]+"-hibridos-mac.png",bbox_inches='tight')
        plt.show()
    
    xt=[]
    yt=[]
    yerrt=[]
    for b in range(len(nomb)):
        if b == 0:
            etiq = u'Tamaño (µm ^ 2)'
        elif b in [1,5,6,7]:
            etiq = u'Tamaño (µm)'
        else:
            etiq = ''
        
        if(yaque[b][5]!=[] or yaque[b][11]!=[] or yaque[b][17]!=[]):
            lista+=yaque[b][5]
            lista+=yaque[b][11]
            lista+=yaque[b][17]
            x_aux.append('Yaque')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista1=lista
            lista=[]

        if(Rayado[b][5]!=[] or Rayado[b][11]!=[] or Rayado[b][17]!=[]):
            lista+=Rayado[b][5]
            lista+=Rayado[b][11]
            lista+=Rayado[b][17]
            x_aux.append('Rayado')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista2=lista
            lista=[]
        
        if(hibri[b][5]!=[] or hibri[b][11]!=[] or hibri[b][17]!=[]):
            lista+=hibri[b][5]
            lista+=hibri[b][11]
            lista+=hibri[b][17]
            x_aux.append('Hibridos')
            y_aux.append(float(statis.median(lista)))
            yerr_aux.append(float(statis.pstdev(lista)))
            lista3=lista
            lista=[]

        plt.hist([lista1,lista2,lista3], label=['Yaque','Rayado','Hibridos'])
        plt.legend(loc='upper right')
        plt.title(nomb[b]+' Histograma Machos Mayo 2017')
        plt.savefig(raiz+"Mac-histo-"+nomb[b]+".png",bbox_inches='tight')
        plt.show()

        lista1=[]
        lista2=[]
        lista3=[]
    
        xt.append(x_aux)
        yt.append(y_aux)
        yerrt.append(yerr_aux)
        x_aux=[]
        y_aux=[]
        yerr_aux=[]

    for b in range(len(nomb)):

        if b == 0:
            etiq = u'Tamaño (µm ^ 2)'
        elif b in [1,5,6,7]:
            etiq = u'Tamaño (µm)'
        else:
            etiq = ''

        plt.errorbar(xt[b], yt[b], yerrt[b], marker='s', ecolor='green', mfc='black',mec='green')
        plt.title(nomb[b]+' Medias Machos Mayo 2017')
        plt.ylabel(etiq)
        plt.savefig(raiz+nomb[b]+"-medias-mac.png",bbox_inches='tight')
        plt.show()
        
class MiClase:
    def __init__(self):
        print("una clase")